<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 30.07.2015
 * Time: 6:01
 */

require_once 'protected/components/SimpleImage.php';



$image = new SimpleImage();
$image->load($_SERVER['DOCUMENT_ROOT'] . $_GET['filename']);

if(isset($_GET['width']) && isset($_GET['height']))
    $image->resize($_GET['width'], $_GET['height']);

elseif (isset($_GET['width']) && !isset($_GET['height']))
    $image->resizeToWidth($_GET['width']);

elseif (!isset($_GET['width']) && isset($_GET['height']))
    $image->resizeToWidth($_GET['height']);
else {
    header('Content-Type: text/html');
    die('error');
}

header('Content-Type: image/jpeg');
$image->output();