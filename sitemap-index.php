<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 27.07.2015
 * Time: 0:06
 */

header('Content-type: application/xml');

$directory         = 'sitemaps';
$scanned_directory = array_diff(scandir($directory), array('..', '.'));

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9                                     http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">
    <?foreach($scanned_directory as $sitemap):?>
        <sitemap><loc>http://lawato.com/sitemaps/<?=$sitemap?></loc><lastmod><?=date('c', filemtime('sitemaps/'.$sitemap)); ?></lastmod></sitemap>
    <?endforeach;?>
</sitemapindex>