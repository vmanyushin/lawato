<?php

class UsersController extends AdminController
{
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'created_at DESC';

		$pages = new CPagination(User::model()->count());

		$pages->pageSize = 500;

		$pages->applyLimit($criteria);

		$items = User::model()->findAll($criteria);

		$this->render('index', array(
			'pages' => $pages,
			'items' => $items
		));
	}

    public function actionShow($id = null)
    {
        $this->render('show', array(
            'model' => User::model()->findByPk($id),
        ));
    }

	public function actionDelete()
	{
		$id = (int)$_GET['id'];
		$model = User::model()->findByPk($id);
		$model->delete();

		Review::model()->deleteAllByAttributes(array(
			"user_id" => $id
		));

		$this->redirect("/admin/users/");
	}

	public function actionEdit()
	{
		$id = (int)$_GET['id'];

		$model = User::model()->findByPk($id);

		if(isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			if( $model->save() ) {
				$this->redirect("/admin/users/");
			}
		}

		$this->render("edit", array(
			"model" => $model,
		));
	}
}