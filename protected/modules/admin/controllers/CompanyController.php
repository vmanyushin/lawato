<?php

class CompanyController extends AdminController
{
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'id DESC';

		$pages = new CPagination(NewCompany::model()->count());

		$pages->pageSize = 50;

		$pages->applyLimit($criteria);

		$items = NewCompany::model()->findAll($criteria);

		$this->render('index', array(
			'pages' => $pages,
			'items' => $items,
		));
	}

	public function actionDelete()
	{
		$id = (int)$_GET['id'];
		$model = NewCompany::model()->findByPk($id);
		$model->delete();

		$this->redirect("/admin/company/");
	}

	public function actionApprove()
	{
		$id = (int)$_GET['id'];
		$obj = NewCompany::model()->findByPk($id);

		if( $obj->id > 0 ) {
			$model = Company::model()->findByPk($obj->id);
			$oldName = $model->name;
			$isNew = false;
		} else {
			$model = new Company;
			$isNew = true;
		}

		$model->attributes = $obj->attributes;
		$model->user_id = $obj->user_id;

		unset($model->votes_num, $model->votes_summ);

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <noreply@lawato.com>' . "\r\n";
        $headers .= 'Cc: noreply@lawato.com' . "\r\n";


		if( $model->save() ) {
			$user = User::model()->findByPk($obj->user_id);
			if( $isNew ) {
                $message = "
                <html>
                    <body>
                        <p>
                            You create a company profile on our site \"{$model->name}\".
                            We approved it. It is available via this <a href='http://lawato.com/".$model->id . '-' . Utils::slugify($model->name) ."'>link</a>.
                        </p>
                        <p>
                        	You can also write a review about this company <a href='http://lawato.com/comment?company_id={$model->id}'>here</a>
                        </p>
                    </body>
                </html>
                ";
			} else {
                $message = "
                <html>
                    <body>
                        <p>
                            Previously, you have changed the profile of the company \"{$oldName}\". 
                            We approved it. It is available via this <a href='http://lawato.com/".$model->id . '-' . Utils::slugify($model->name) ."'>link</a>.
                        </p>
                    </body>
                </html>
                ";
			}

			mail($user->email, "Company approved", $message, $headers);
			$obj->delete();
            $this->redirect("/admin/company/");
		} else {
            Yii::app()->user->setFlash('error', $model->getErrors());
        }

		$this->redirect("/admin/company/");
	}

}