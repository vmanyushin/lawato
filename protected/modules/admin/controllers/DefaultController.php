<?php

class DefaultController extends AdminController
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionLogin()
	{
		$this->layout = '//layouts/main';

		if(isset($_POST['pwd']) && $_POST['pwd'] == Yii::app()->params['admin']['password']) {
			$_SESSION['is_admin'] = 1;
			$this->redirect("/admin/default/index");
		}

		$this->render('login');
	}

	public function actionLogout()
	{
		unset($_SESSION['is_admin']);
		$this->redirect("/admin/default/login");
	}
}