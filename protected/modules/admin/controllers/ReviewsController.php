<?php

class ReviewsController extends AdminController
{
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'created_at DESC';

		$pages = new CPagination(Review::model()->count());

		$pages->pageSize = 50;

		$pages->applyLimit($criteria);

		$items = Review::model()->findAll($criteria);

		$this->render('index', array(
			'pages' => $pages,
			'items' => $items
		));
	}

	public function actionDelete()
	{
		$id = (int)$_GET['id'];
		$model = Review::model()->findByPk($id);

        $votes = Votes::model()->findByPk($model->company_id);

        if($votes) {
            $votes->num -= 1;
            $votes->sum -= $model->grade;
            $votes->save();
        }

		$model->delete();
		$this->redirect("/admin/reviews/");
	}

	public function actionEdit()
	{
		$id = (int)$_GET['id'];

		$model = Review::model()->findByPk($id);

		if(isset($_POST['Review'])) {
			$model->attributes = $_POST['Review'];

			if( $model->save() ) {
				$this->redirect("/admin/reviews/");
			}
		}

		$this->render("edit", array(
			"model" => $model,
		));
	}
}