<?php echo CHtml::form('','post',array('enctype'=>'multipart/form-data', 'class' => 'form-horizontal')); ?>
  <?php echo CHtml::errorSummary($model); ?>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Name</label>
    <div class="controls">
      <?php echo CHtml::activeTextField($model,'full_name'); ?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Birthday</label>
    <div class="controls">
      <?php echo CHtml::activeTextField($model,'birthday'); ?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Position</label>
    <div class="controls">
      <?php echo CHtml::activeTextField($model,'position'); ?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Register date</label>
    <div class="controls">
      <?php echo CHtml::activeTextField($model,'created_at'); ?>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn">Сохранить</button>
    </div>
  </div>
</form>