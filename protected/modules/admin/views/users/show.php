<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 29.08.2015
 * Time: 22:48
 */
?>

<h3>User profile</h3>
<div style="position: relative; width: 100%;">
<img id="" class="img-thumbnail" src="<?= $model->photo ?>" width="96" style="margin-bottom: 10px"/>
</div>
<table class="table table-bordered">
    <tbody>
        <tr>
            <td><strong>Full name:</strong></td>
            <td><?= $model->full_name ?></td>
        </tr>

        <tr>
            <td><strong>Email:</strong></td>
            <td><?= $model->email ?></td>
        </tr>

        <tr>
            <td><strong>Birthday:</strong></td>
            <td><?= $model->birthday ?></td>
        </tr>

        <tr>
            <td><strong>Age:</strong></td>
            <td><?= $model->age ?></td>
        </tr>

        <tr>
            <td><strong>Position:</strong></td>
            <td><?= $model->position ?></td>
        </tr>

        <tr>
            <td><strong>Sex:</strong></td>
            <td><?= $model->gender == 'F' ? 'Female' : ' Male' ?></td>
        </tr>
    </tbody>
</table>

<h4>List companies owned by this user</h4>
<?php foreach(Company::model()->findAllByAttributes(['user_id' => $model->id]) as $company): ?>
    <p><a href="<?= $company->urlpath ?>"><?= $company->name ?></a></p>
<?php endforeach ?>

<h4>List review</h4>
<?php foreach(Review::model()->findAllByAttributes(['user_id' => $model->id]) as $review): ?>
    <p><a href="<?= $review->company->urlpath ?>#review"><?= $review->company->name ?></a></p>
<?php endforeach ?>
