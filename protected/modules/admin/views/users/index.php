<table class="table table-bordered">
	<thead>
		<tr>
			<th>
				E-mail
			</th>
			<th>
				имя
			</th>
			<th>
				пол
			</th>
			<th>
				возраст
			</th>
			<th>
				Reg date
			</th>
			<th>
				Social
			</th>
			<th>
				&nbsp;
			</th>
		</tr>
	</thead>
	<tbody>
		<? foreach($items as $item): ?>
			<tr>
				<td>
					<a href="/admin/users/show/<?= $item->id ?>" target="_blank"><?= $item->email ?></a>
				</td>
				<td>
					<?= $item->full_name ?>
				</td>
				<td>
					<?= $item->gender ?>
				</td>
				<td>
					<?= $item->birthday ?>
				</td>
				<td>
					<?= $item->created_at ?>
				</td>
				<td>
					<? if($item->identity): ?>
						<a target="_blank" href="<?=$item->identity?>">link</a>
					<? else: ?>
						<i class="icon-remove"></i>
					<? endif; ?>
				</td>
				<td>
					<a class="btn btn-default" href="/admin/users/edit?id=<?=$item->id?>">Редактировать</a>
					<a class="btn btn-default" href="/admin/users/delete?id=<?=$item->id?>">Удалить</a>	
				</td>
			</tr>
		<? endforeach; ?>
	</tbody>
</table>