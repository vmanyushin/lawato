<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    if($key = 'success') {
        echo '<div class="alert alert-success" role="alert">' . $key . ' ' .  $message . '</div>';
        return;
    }
    foreach($message as $key_msg => $msg)
        echo '<div class="alert alert-warning" role="alert">' . $key . ' ' .  $msg[0] . '</div>';
}
?>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>
				Mode
			</th>
			<th>
				Name
			</th>
			<th>
				User
			</th>
			<th>
				Edit date
			</th>
			<th>
				&nbsp;
			</th>
		</tr>
	</thead>
	<tbody>
		<? foreach($items as $item): ?>
			<tr>
				<td>
					<? if($item->id): ?>
						<b>UPDATE</b>
					<? else: ?>
						<b>NEW</b>
					<? endif; ?>
				</td>
				<td>
					<a href="/company/preview/<?= $item->id ?>" target="_blank"><?= $item->name ?></a>
				</td>
				<td>
					<?if(isset($item->user)):?> <a href="/admin/users/show/<?= $item->user->id ?>" target="_blank"><?= $item->user->email ?></a><?else:?>&nbsp;<?endif;?>
				</td>
				<td>
					<?= $item->created_at ?>
				</td>
				<td>
					<a class="btn btn-default" href="/admin/company/approve?id=<?=$item->id?>">Approve</a>
					<a class="btn btn-default" href="/admin/company/delete?id=<?=$item->id?>">Decline</a>
					<a class="btn btn-default" target="_blank" href="/company/edit/<?= $item->id ?>">Edit</a>
				</td>
			</tr>
		<? endforeach; ?>
	</tbody>
</table>