<?php
/* @var $this CompanyController */
/* @var $model BusinessNew */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'business-new-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="rowq">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?if($model->logo):?>
			<img src="/ups<?=$model->logo?>" width="250px">
			<br>
		<?endif;?>
		<?php echo $form->textField($model,'logo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'division'); ?>
		<?php echo $form->textField($model,'division',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'division'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'nearest_landmarks'); ?>
		<?php echo $form->textField($model,'nearest_landmarks',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nearest_landmarks'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'extended_zip_code'); ?>
		<?php echo $form->textField($model,'extended_zip_code',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'extended_zip_code'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'zip_code'); ?>
		<?php echo $form->textField($model,'zip_code',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'zip_code'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'contact_name'); ?>
		<?php echo $form->textField($model,'contact_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contact_name'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'contact_title'); ?>
		<?php echo $form->textField($model,'contact_title',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contact_title'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'contact_phone'); ?>
		<?php echo $form->textField($model,'contact_phone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'contact_phone'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'contact_description'); ?>
		<?php echo $form->textField($model,'contact_description',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'contact_description'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'website'); ?>
		<?php echo $form->textField($model,'website',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'website'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'detailed_description'); ?>
		<?php echo $form->textArea($model,'detailed_description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'detailed_description'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'employees'); ?>
		<?php echo $form->textField($model,'employees',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'employees'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'annual_sales'); ?>
		<?php echo $form->textField($model,'annual_sales',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'annual_sales'); ?>
	</div>

	<div class="rowq">
		<?php echo $form->labelEx($model,'youtube'); ?>
		<?php echo $form->textField($model,'youtube',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'youtube'); ?>
	</div>


	<div class="rowq buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->