<?php
/* @var $this CompanyController */
/* @var $model BusinessNew */

$this->breadcrumbs=array(
	'Business News'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BusinessNew', 'url'=>array('index')),
	array('label'=>'Manage BusinessNew', 'url'=>array('admin')),
);
?>

<h1>Create BusinessNew</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>