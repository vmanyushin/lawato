<?php
/* @var $this CompanyController */
/* @var $model BusinessNew */

$this->breadcrumbs=array(
	'Business News'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List BusinessNew', 'url'=>array('index')),
	array('label'=>'Create BusinessNew', 'url'=>array('create')),
	array('label'=>'Update BusinessNew', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BusinessNew', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BusinessNew', 'url'=>array('admin')),
);
?>

<h1>View BusinessNew #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'origin_id',
		'title',
		'logo',
		'images',
		'division',
		'address',
		'nearest_landmarks',
		'extended_zip_code',
		'zip_code',
		'phone',
		'description',
		'contact_name',
		'contact_title',
		'contact_phone',
		'contact_description',
		'website',
		'email',
		'types',
		'category_id',
		'sub_category_id',
		'state_id',
		'city_id',
		'votes_num',
		'votes_summ',
		'brief_description',
		'detailed_description',
		'employees',
		'annual_sales',
		'products',
		'brands',
		'youtube',
		'socials',
		'phones',
		'timetable',
	),
)); ?>
