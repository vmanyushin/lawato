<?php
/* @var $this CompanyController */
/* @var $data BusinessNew */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origin_id')); ?>:</b>
	<?php echo CHtml::encode($data->origin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
	<?php echo CHtml::encode($data->logo); ?>
	<br />
	<?/*?>
	<b><?php echo CHtml::encode($data->getAttributeLabel('images')); ?>:</b>
	<?php echo CHtml::encode($data->images); ?>
	<br />
	<?*/?>
	<b><?php echo CHtml::encode($data->getAttributeLabel('division')); ?>:</b>
	<?php echo CHtml::encode($data->division); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nearest_landmarks')); ?>:</b>
	<?php echo CHtml::encode($data->nearest_landmarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extended_zip_code')); ?>:</b>
	<?php echo CHtml::encode($data->extended_zip_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zip_code')); ?>:</b>
	<?php echo CHtml::encode($data->zip_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_name')); ?>:</b>
	<?php echo CHtml::encode($data->contact_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_title')); ?>:</b>
	<?php echo CHtml::encode($data->contact_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_phone')); ?>:</b>
	<?php echo CHtml::encode($data->contact_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_description')); ?>:</b>
	<?php echo CHtml::encode($data->contact_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('types')); ?>:</b>
	<?php echo CHtml::encode($data->types); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::encode($data->category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->sub_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state_abbreviation')); ?>:</b>
	<?php echo CHtml::encode($data->state_abbreviation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city_id')); ?>:</b>
	<?php echo CHtml::encode($data->city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('votes_num')); ?>:</b>
	<?php echo CHtml::encode($data->votes_num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('votes_summ')); ?>:</b>
	<?php echo CHtml::encode($data->votes_summ); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brief_description')); ?>:</b>
	<?php echo CHtml::encode($data->brief_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detailed_description')); ?>:</b>
	<?php echo CHtml::encode($data->detailed_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('employees')); ?>:</b>
	<?php echo CHtml::encode($data->employees); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('annual_sales')); ?>:</b>
	<?php echo CHtml::encode($data->annual_sales); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('products')); ?>:</b>
	<?php echo CHtml::encode($data->products); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brands')); ?>:</b>
	<?php echo CHtml::encode($data->brands); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('youtube')); ?>:</b>
	<?php echo CHtml::encode($data->youtube); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socials')); ?>:</b>
	<?php echo CHtml::encode($data->socials); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phones')); ?>:</b>
	<?php echo CHtml::encode($data->phones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timetable')); ?>:</b>
	<?php echo CHtml::encode($data->timetable); ?>
	<br />

	*/ ?>

</div>