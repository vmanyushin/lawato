<?php
/* @var $this CompanyController */
/* @var $model BusinessNew */

$this->breadcrumbs=array(
	'Business News'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BusinessNew', 'url'=>array('index')),
	array('label'=>'Create BusinessNew', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#business-new-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Business News</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'business-new-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'origin_id',
		'title',
		'logo',
		'images',
		'division',
		/*
		'address',
		'nearest_landmarks',
		'extended_zip_code',
		'zip_code',
		'phone',
		'description',
		'contact_name',
		'contact_title',
		'contact_phone',
		'contact_description',
		'website',
		'email',
		'types',
		'category_id',
		'sub_category_id',
		'state_id',
		'city_id',
		'votes_num',
		'votes_summ',
		'brief_description',
		'detailed_description',
		'employees',
		'annual_sales',
		'products',
		'brands',
		'youtube',
		'socials',
		'phones',
		'timetable',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
