<table class="table table-bordered">
	<thead>
		<tr>
			<th>
				Bad
			</th>
			<th>
				компания
			</th>
			<th>
				дата
			</th>
			<th>
				логин
			</th>
			<th>
				approved
			</th>
			<th>
				&nbsp;
			</th>
		</tr>
	</thead>
	<tbody>
		<? foreach($items as $item): ?>
			<tr>
				<td>
				</td>
				<td>
					<a href="/<?=$item->company->id?>-<?= Utils::slugify($item->company->name)?>#review" target="_blank"><?= $item->company->name ?></a>
				</td>
				<td>
					<?= $item->created_at ?>
				</td>
				<td>
					<a href="/admin/users/show/<?= $item->user->id ?>" target="_blank"><?= $item->user->email ?></a>
				</td>
				<td>
					<? if($item->approved): ?>
						<i class="icon-ok"></i>
					<? else: ?>
						<i class="icon-remove"></i>
					<? endif; ?>
				</td>
				<td>
					<a class="btn btn-default" href="/admin/reviews/edit?id=<?=$item->id?>">Редактировать</a>
					<a class="btn btn-default" href="/admin/reviews/delete?id=<?=$item->id?>">Удалить</a>	
				</td>
			</tr>
		<? endforeach; ?>
	</tbody>
</table>