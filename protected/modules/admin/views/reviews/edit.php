<?php echo CHtml::form('','post',array('enctype'=>'multipart/form-data', 'class' => 'form-horizontal')); ?>
  <?php echo CHtml::errorSummary($model); ?>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Header</label>
    <div class="controls">
      <?php echo CHtml::activeTextField($model,'header'); ?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Content</label>
    <div class="controls">
      <?php echo CHtml::activeTextArea($model,'content', array( "style" => "width:430px;height:260px" )); ?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Grade</label>
    <div class="controls">
      <?php echo CHtml::activeTextField($model,'grade'); ?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Approved</label>
    <div class="controls">
      <?php echo CHtml::activeCheckBox($model,'approved'); ?>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn">Сохранить</button>
    </div>
  </div>
</form>