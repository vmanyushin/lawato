<?php
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
//		'gii'=>array(
//			'class'=>'system.gii.GiiModule',
//			'password' => 'qwerty',
//			'ipFilters'=>array('127.0.0.1','::1'),
//		),
        'admin',
	),

	// application components
	'components'=>array(
        'session' => array(
            'class' => 'CCacheHttpSession',
            'cacheID' => 'sessionCache',
        ),

        'sessionCache' => array(
            'class' => 'CMemCache',
        ),

        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),

		'user'=>array(
			'allowAutoLogin'=>true,
		),

		'urlManager'=>array(
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'rules'=>array(
                '<id:(\d+)>-<slug:[a-z0-9-_]+>'                    => 'company',
                '<id:(\d+)>-'                                      => 'company',
                '/company/<preview:preview>/<id:(\d+)>'            => 'company',

				'category/<category:(\d+)>-<slug:[a-z0-9\-\_]+>'   => 'category',
                'cat-<category:(\d+)>-[a-z0-9\-\_]+'               => 'category',
                'subcat-<subcategory:(\d+)>-[a-z0-9\-\_]+'         => 'category',
                'subcat-<subcategory:(\d+)>-[a-z0-9\-\_]+/page/<page:(\d+)>' => 'category',

				'state/<state:(\d+)>-<slug:[a-z0-9\-\_]+>'         => 'category',

                'state/<state:(\d+)>/city/<city:(\d+)>/category/<category:(\d+)>/subcategory/<subcategory:(\d+)>' => 'category',
                'state/<state:(\d+)>/city/<city:(\d+)>/category/<category:(\d+)>' => 'category',
                'state/<state:(\d+)>/city/<city:(\d+)>/'           => 'category',
                'state/<state:(\d+)>/category/<category:(\d+)>/subcategory/<subcategory:(\d+)>' => 'category',
                'state/<state:(\d+)>/category/<category:(\d+)>'    => 'category',

                '/category/<category:(\d+)>/subcategory/<subcategory:(\d+)>' => 'category',
                '/category/<category:(\d+)>'                       => 'category',

                '\w{2}-<state:(\d+)>-[a-z0-9\-\_]+'                => 'category',

				'city/<city:(\d+)>-[a-z0-9\-\_]+'                  => 'category',
                'city-<city:(\d+)>-[a-z0-9\-\_]+'                  => 'category',

                'ajax/company'                                     => 'site/company',
                'ajax/city'                                        => 'site/city',
                'ajax/setlocation'                                 => 'site/setlocation',
                'search'                                           => 'site/search',
                'review'                                           => 'site/addreview',
                'company/edit/<id:(\d+)>'                          => 'company/edit',

                'company/uploadcompanylogo/<id:(\d+)>'             => 'company/uploadcompanylogo',
                'company/deletecompanylogo/<id:(\d+)>'             => 'company/deletecompanylogo',
                'company/uploadcompanyimage/<id:(\d+)>'            => 'company/uploadcompanyimage',
                'company/deletecompanyimage/<id:(\d+)>/<img:(.*)>' => 'company/deletecompanyimage',

                '/admin/<controller:\w+>/<action:\w+>/<id:(\d+)>'  => '/admin/<controller>/<action>',
			),
		),

		'db'     => require(dirname(__FILE__).'/database.php'),
        'sphinx' => require(dirname(__FILE__).'/sphinx.php'),

		'errorHandler'=>array(
			//'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
//				array(
//					'class'=>'CFileLogRoute',
//					'levels'=>'error, warning',
//				),
				array(
					'class'=>'CWebLogRoute',
					'levels'=>'error, warning, info',
				),
//                array(
//                    'class'=>'application.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
//                    'ipFilters'=>array('127.0.0.1','192.168.0.13'),
//                ),
			),
		),

	),

	// using Yii::app()->params['paramName']
	'params'=>array(
		'adminEmail'=>'admin@lawato.com',

        'admin' => array(
            'password' => 'adminka'
        ),

        'logoPath'   => '/images/logo/',
        'uploadPath' => '/uploads/',
    ),
);
