<?php

// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=localhost;dbname=lawato-main',
	'emulatePrepare' => true,
	'username' => 'lawato',
	'password' => 'dgh67u8jh56y5t',
	'charset' => 'utf8',
    'enableProfiling' => true,
    'enableParamLogging' => true,
    'schemaCachingDuration' => 0,
);