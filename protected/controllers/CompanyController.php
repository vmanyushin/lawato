<?php

/**
 * Created by PhpStorm.
 * User: swop
 * Date: 25.07.2015
 * Time: 1:21
 */
class CompanyController extends Controller
{
    private $errorMsg;
    private $uploadedFileName;

    public function init()
    {
        parent::init();

        $_SESSION['REFERER'] = $_SERVER['REQUEST_URI'];
    }

    public function actionIndex($id = '', $slug = '', $preview = false)
    {
        $params = compact('id', 'state', 'city', 'category', 'subcategory', 'slug');

        if ($preview == false) $model = Company::model();
        else $model = NewCompany::model();

        return $this->render('index', [
            'params' => $params,
            'item' => $model->findByPk($id),
            'reviews' => Review::model()->approved()->withCompany($id)->findAll(),
            'votes' => Votes::model()->findByPk($id),
        ]);
    }

    /**
     * Show all companys that user owned
     */
    public function actionShow()
    {
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(403, 'authorization required');
        }

        $this->render('show', [
            'model' => Company::model()->findAll(
                [
                    'condition' => 'user_id = :user_id AND status = 1',
                    'params' => [
                        'user_id' => Yii::app()->user->id
                    ],
                ]
            )]);
    }

    /**
     * create new company
     */
    public function actionCreate()
    {
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(403, 'authorization required');
        }

        $form = new CompanyForm();

        if (isset($_POST['CompanyForm'])) {
            $form->attributes = $_POST['CompanyForm'];

            if (!$form->validate())
                return $this->render('create', ['form' => $form]);

            $company = new Company();

            $company->name = $form->name;
            $company->status = 0;
            $company->state_id = $form->state_id;
            $company->city_id = $form->city_id;
            $company->address = $form->address;
            $company->phones = $form->phones;
            $company->fax = $form->fax;
            $company->user_id = Yii::app()->user->id;

            if ($company->save()) {

                $votes = new Votes();
                $votes->id  = $company->id;
                $votes->num = 0;
                $votes->sum = 0;
                $votes->save();

                return $this->redirect('/company/edit/' . $company->id);
            }
            else
                Yii::app()->user->setFlash('error', $this->formatError($company));

        }

        return $this->render('create', ['form' => $form]);
    }

    /**
     * Edit exists company
     *
     * @throws CHttpException 404 when company not found
     * @throws CHttpException 403 when user not authorized
     *
     * @param (integer) company id
     *
     * @return edit View
     */
    public function actionEdit($id = null)
    {
        if ($id == null)
            throw new CHttpException(404, 'page not found ' . $id);

        $form = new CompanyForm();
        $model = Company::model()->findByPk($id);
        $tempModel = NewCompany::model()->findByAttributes(['id' => $id]);
        $categories = [];

        if (Yii::app()->user->isGuest) {
            throw new CHttpException(200, 'authorization required');
        }

        if ($model->user_id == null) {
            $model->user_id = Yii::app()->user->id;
            $model->save();
        }

        if (isset($_POST['CompanyForm']) && $tempModel) {

            $persons = [];

            if(isset($_POST['CompanyForm']['personal'])) {

                foreach($_POST['CompanyForm']['personal'] as $person) {
                    $persons[] = [$person['url'], $person['name'], $person['position']];
                }

                $_POST['CompanyForm']['personal'] = $persons;
            }

            if (isset($_POST['CompanyForm']['categories'])) {

                CompanySubcategory::model()->deleteAllByAttributes(['company_id' => $id]);

                foreach ($_POST['CompanyForm']['categories'] as $subCategory) {
                    $category = new CompanySubcategory();
                    $category->company_id = $id;
                    $category->subcategory_id = $subCategory;
                    $category->save();

                    $categories[] = $subCategory;
                }
            }

            unset($_POST['CompanyForm']['categories']);

            $form->attributes = $_POST['CompanyForm'];

            if (!$form->validate()) {
                echo print_r($form->getErrors());
                return $this->render('edit', ['model' => $form]);
            }

            $images = $tempModel->images;
            $logo = $tempModel->logo;

            $workHours = new WorkHours($_POST['CompanyForm']['hours']);
            $form->hours = $workHours->toString();
            $form->categories = null;

            $tempModel->attributes = array_filter($form->attributes);
            $form->id = $tempModel->id = $id;
            $form->images = $tempModel->images = $images;
            $form->logo = $tempModel->logo = $logo;
            $form->hours = $workHours;
            $form->categories = $categories;

            if (!$tempModel->save()) {
                return $this->render('edit', ['model' => $form]);
            }

            $this->redirect('/company/preview/' . $id);

        } else if ($tempModel) {
            $form->attributes = $tempModel->attributes;

            foreach (CompanySubcategory::model()->findAllByAttributes(['company_id' => $id]) as $category)
                $form->categories[] = $category->subcategory_id;

        } else {
            $model->updated_at = null;

            $tempModel         = new NewCompany();
            $tempModel->attributes = array_filter($model->attributes);
            $tempModel->id     = $model->id;
            $tempModel->hours  = new WorkHours();

            foreach (CompanySubcategory::model()->findAllByAttributes(['company_id' => $id]) as $category)
                $form->categories[] = $category->subcategory_id;

            $tempModel->save();
            $this->refresh();
        }

        return $this->render('edit', ['model' => $form]);
    }

    /**
     * Find cities from State by given state_id and print them
     *
     * @param (integer) state_id
     */
    public function actionLoadCities()
    {
        echo '<option>City</option>';

        foreach (City::model()->findAll(['condition' => 'state_id = :state_id', 'params' => [':state_id' => Yii::app()->request->getParam('id')]]) as $city) {
            echo "<option value='$city->id'>$city->name</option>";
        }
    }

    public function actionUploadCompanyLogo($id = null)
    {
        $user = Yii::app()->user->id;

        // TODO: подобное заменить на RBAC
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(404, 'authorization required');
        }

        $model = NewCompany::model()->findByAttributes(['user_id' => $user, 'id' => $id]);
        if (!$model) {
            throw new CHttpException(404, 'not found');
        }

        if ($this->saveUploadedFile('upload-logo', Yii::app()->params['logoPath'])) {
            $model->logo = $this->uploadedFileName;
            $model->save();

            $this->responseJson(['status' => 'success', 'path' => Yii::app()->params['logoPath'] . $model->logo]);
        }

        $this->responseJson(['status' => 'error', 'reason' => $this->errorMsg]);
    }

    public function actionDeleteCompanyLogo($id = null)
    {
        $user = Yii::app()->user->id;

        // TODO: подобное заменить на RBAC
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(404, 'authorization required');
        }

        $model = NewCompany::model()->findByAttributes(['user_id' => $user, 'id' => $id]);
        if (!$model) {
            throw new CHttpException(404, 'not found');
        }

        $model->logo = '';
        $model->save();

        $this->responseJson(['status' => 'success']);
    }

    public function actionUploadCompanyImage($id = null)
    {
        $user = Yii::app()->user->id;

        // TODO: подобное заменить на RBAC
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(404, 'authorization required');
        }

        $model = NewCompany::model()->findByAttributes(['user_id' => $user, 'id' => $id]);
        if (!$model) {
            throw new CHttpException(404, 'not found');
        }

        $path = $model->images;
        $FILES = $_FILES['upload-image'];
        unset($_FILES);

        $count = count($FILES['name']);

        for ($i = 0; $i < $count; $i++) {
            $_FILES['upload-image'] = [
                'name' => $FILES['name'][$i],
                'type' => $FILES['type'][$i],
                'tmp_name' => $FILES['tmp_name'][$i],
                'error' => $FILES['error'][$i],
                'size' => $FILES['size'][$i],
            ];

            if ($this->saveUploadedFile('upload-image', Yii::app()->params['uploadPath'])) {
                $path[] = $this->uploadedFileName;
            } else {
                $this->responseJson(['status' => 'error', 'reason' => $this->errorMsg]);
            }
            unset($_FILES);
        }

        $model->images = $path;
        $model->save();

        foreach ($path as &$image) {
            $image = Yii::app()->params['uploadPath'] . $image;
        }
        $this->responseJson(['status' => 'success', 'path' => $path]);
    }

    public function actionDeleteCompanyImage($id = null, $img = null)
    {
        $user = Yii::app()->user->id;

        // TODO: подобное заменить на RBAC
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(404, 'authorization required');
        }

        $model = NewCompany::model()->findByAttributes(['user_id' => $user, 'id' => $id]);
        if (!$model) {
            throw new CHttpException(404, 'not found');
        }

        if (count($model->images) > 0 && isset($img)) {
            $images = $model->images;
            array_splice($images, $img, 1);
            $model->images = $images;
            $model->save();
        }

        foreach ($images as &$image) {
            $image = Yii::app()->params['uploadPath'] . $image;
        }
        $this->responseJson(['status' => 'success', 'path' => $images]);
    }

    private function responseJson($data)
    {
        header('Content-type: application/json');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);

        Yii::app()->end();
    }

    private function saveUploadedFile($name, $path = '/uploads/')
    {
        try {
            if (!isset($_FILES[$name]['error']) || is_array($_FILES[$name]['error'])) {
                throw new RuntimeException('Invalid parameters: ' . $_FILES[$name]['error']);
            }

            switch ($_FILES[$name]['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            if ($_FILES[$name]['size'] > 10000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            if (exif_imagetype($_FILES[$name]['tmp_name']) === false) {
                throw new RuntimeException('Invalid file format.');
            }

            $ext = pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);

            $this->uploadedFileName = sha1_file($_FILES[$name]['tmp_name']) . '.' . $ext;
            if (!move_uploaded_file($_FILES[$name]['tmp_name'],
                sprintf('%s%s', $_SERVER['DOCUMENT_ROOT'] . $path, $this->uploadedFileName))
            ) {
                throw new RuntimeException('Failed to move uploaded file.');
            }

            return true;
        } catch (RuntimeException $e) {
            $this->errorMsg = $e->getMessage();
            return false;
        }
    }
} 