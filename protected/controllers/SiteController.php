<?php

class SiteController extends Controller
{
    public $bodyClass;

    public function actionIndex()
    {
        $states = State::model()->findAll();
        $cities = City::model()->findAll(['limit' => 30]);
        $categories = Category::model()->findAll();

        $this->render('index', [
            'states' => $states,
            'cities' => $cities,
            'categories' => $categories
        ]);
    }

    public function actionSearch()
    {
        $search = [
            'location' => Yii::app()->request->getParam('location'),
            'name' => Yii::app()->request->getParam('name'),
        ];

        if ($search['location'] == '' && isset(Yii::app()->session['search']['location']))
            $search['location'] = Yii::app()->session['search']['location'];

        if ($search['name'] == '' && isset(Yii::app()->session['search']['name']))
            $search['name'] = Yii::app()->session['search']['name'];

        Yii::app()->session['search'] = $search;

        $location = explode(', ', $search['location']);
        $name = $search['name'];
        $items = [];

        $city = City::model()->with('state')->find([
            'condition' => 't.name = :name AND state.abbreviation = :abbreviation',
            'params' => [':name' => $location[0], ':abbreviation' => $location[1]]
        ]);

        $query = "SELECT * FROM lawato_company WHERE MATCH('@company {$name}') AND cid = {$city->id} AND sid = {$city->state_id}";
        if ($this->page > 0)
            $query .= " LIMIT " . ($this->page - 1) * $this->pageSize . ',' . $this->pageSize;
        else
            $query .= " LIMIT " . $this->pageSize;

        $command = Yii::app()->sphinx->createCommand($query);
        $result = $command->queryAll();

        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';

        $pages = new CPagination($this->found());
        $pages->pageSize = $this->pageSize;
        $pages->applyLimit($criteria);

        foreach ($result as $item) {
            $items[] = Company::model()->findByPk($item['id']);
        }

        return $this->render('search', ['items' => $items, 'city' => $city, 'pages' => $pages]);
    }


    private function found()
    {
        $meta = Yii::app()->sphinx->createCommand('SHOW META')->queryAll();
        $count = $total = 0;

        foreach ($meta as $row) {
            if ($row['Variable_name'] == 'total') $count = $row['Value'];
            if ($row['Variable_name'] == 'total_found') $total = $row['Value'];
        }

        return $count;
    }

    public function actionCompany()
    {
        $query = explode('#', Yii::app()->getRequest()->getParam('query', ''));
        $location = explode('=', $query[1]);
        $items = [];
        $qry = '';

        if (!empty($location[1])) {
            $address = explode(', ', $location[1]);

            $city = City::model()->with('state')->find([
                'condition' => 't.name = :name AND state.abbreviation = :abbreviation',
                'params' => [':name' => $address[0], ':abbreviation' => $address[1]]
            ]);

            $cid = $city->id;
            $sid = $city->state_id;

            $qry = "SELECT * FROM lawato_company WHERE MATCH('@company {$query[0]}') AND cid = {$cid} AND sid = {$sid}";
            $items = Yii::app()->sphinx->createCommand($qry)->queryAll();
        } else {
            $qry = "SELECT * FROM lawato_company WHERE MATCH('@company {$query[0]}')";
            $items = Yii::app()->sphinx->createCommand($qry)->queryAll();
        }

        $result = [];

        foreach ($items as $item) {
            $company = Company::model()->findByPk($item['id']);

            if (isset($company)) {

                if ($company->logo == '') $company->logo = 'nologo.png';
                $result[] = ['name' => $company->name, 'logo' => $company->logo, 'url' => '/' . $company->id . '-' . Utils::slugify($company->name)];
            }
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        Yii::app()->end();
    }

    public function actionCity()
    {
        $query = Yii::app()->getRequest()->getParam('query', '*');
        $items = Yii::app()->sphinx->createCommand("SELECT * FROM lawato_city WHERE MATCH('{$query}')")->queryAll();
        $result = [];

        foreach ($items as $item) {
            $city = City::model()->findByPk($item['id']);
            $result[] = $city->name . ', ' . $city->state->abbreviation;
        }

        echo json_encode(array('suggestions' => $result), JSON_UNESCAPED_UNICODE);
        Yii::app()->end();
    }

    public function actionSetLocation()
    {
        $location = ['location' => Yii::app()->request->getParam('location', 'null')];
        Yii::app()->session['search'] = $location;

        echo json_encode(array('location' => Yii::app()->session['search']['location']), JSON_UNESCAPED_UNICODE);
    }

    public function actionLegalIssue()
    {
        $query = Yii::app()->getRequest()->getParam('query', '*');
        $items = Yii::app()->sphinx->createCommand("SELECT * FROM lawato_subcategory WHERE MATCH('{$query}')")->queryAll();
        $result = [];

        foreach ($items as $item) {
            $subcategory = Subcategory::model()->findByPk($item['id']);
            $result[] = $subcategory->name;
        }

        echo json_encode(array('suggestions' => $result), JSON_UNESCAPED_UNICODE);
        Yii::app()->end();
    }

    public function actionAddReview()
    {
        if (!isset($_POST['ReviewForm']) || Yii::app()->user->isGuest) {
            $this->redirect($_SERVER['HTTP_REFERER'] . '#object');
            Yii::app()->end();
        }

        $form = new ReviewForm();
        $form->attributes = $_POST['ReviewForm'];

        if (!$form->validate()) {
            Yii::app()->user->setFlash('error', $this->formatError($form));
            $this->redirect($_SERVER['HTTP_REFERER'] . '#review');
        }

        $review = new Review;
        $review->attributes = $form->attributes;
        $review->user_id = Yii::app()->user->id;

        $votes = Votes::model()->findByPk($review->company_id);

        if($votes) {
            $votes->num += 1;
            $votes->sum += $review->grade;
            $votes->save();
        }

        if ($review->validate() && $review->save())
            Yii::app()->user->setFlash('info', 'Your review has been added after moderation');
        else
            Yii::app()->user->setFlash('error', $this->formatError($review));

        $this->redirect($_SERVER['HTTP_REFERER'] . '#review');
    }

    public function actionError()
    {
        return $this->render('error');
    }

}