<?php

/**
 * Created by PhpStorm.
 * User: swop
 * Date: 26.07.2015
 * Time: 16:40
 */
class UserController extends Controller
{
    private $errorMsg;
    private $uploadedFileName;

    public function actionView()
    {
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(403, 'access denied');
        }

        $userForm = new UserForm();
        $user = User::model()->findByPk(Yii::app()->user->id);

        if (isset($_POST['UserForm'])) {
            $userForm->attributes = $_POST['UserForm'];
            $userForm->logo = $user->photo;

            if ($userForm->validate()) {
                $user->full_name = $userForm->full_name;
                $user->birthday = $userForm->birthday;
                $user->email = $userForm->email;
                $user->gender = $userForm->gender;

                if (!empty($userForm->password))
                    $user->password = $userForm->password;

                $user->save();
                $userForm->password = $userForm->passwordRepeat = '';

                return $this->render('view', ['model' => $userForm]);
            } else {
                return $this->render('view', ['model' => $userForm]);
            }
        }

        $userForm->full_name = $user->full_name;
        $userForm->email = $user->email;
        $userForm->gender = $user->gender;
        $userForm->birthday = $user->birthday;
        $userForm->password = $userForm->passwordRepeat = '';
        $userForm->logo = $user->photo;

        return $this->render('view', ['model' => $userForm]);
    }

    public function actionReview()
    {
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(403, 'access denied');
        }

        return $this->render('review', [
            'model' => Review::model()->findAll
            ([
                    'distinct' => true,
                    'select' => 'company_id',
                    'condition' => 'user_id = :user_id',
                    'params' => [':user_id' => Yii::app()->user->id]
                ])
        ]);
    }

    public function actionUploadLogo()
    {
        if (Yii::app()->user->isGuest)
            $this->responseJson(['status' => 'error', 'reason' => 'auth error']);

        if ($this->saveUploadedFile()) {
            $user = User::model()->findByPk(Yii::app()->user->id);

            if (!$user) {
                $this->responseJson(['status' => 'error', 'reason' => 'user not found']);
            }

            $user->scenario = 'update-logo';
            $user->logo = $this->uploadedFileName;

            if (!$user->save()) {
                $this->responseJson(['status' => 'error', 'reason' => $user->getErrors()]);
            }

            $this->responseJson(['status' => 'success', 'path' => '/uploads/' . $this->uploadedFileName]);
        } else
            $this->responseJson(['status' => 'error', 'reason' => $this->errorMsg]);
    }

    public function actionRegister()
    {
        if (!isset($_POST['RegisterForm']))
            $this->responseJson(['register' => 'error', 'error' => 'no data']);

        $form = new RegisterForm();
        $form->attributes = $_POST['RegisterForm'];

        if (!$form->validate())
            $this->responseJson(['register' => 'error', 'errorMsg' => $this->getFirstError($form)]);

        $user = new User('registration');

        $user->email = $form->email;
        $exist = $user->isExist();

        if ($exist)
            $this->responseJson(['register' => 'error', 'errorMsg' => 'User with this email already registered']);

        $user->full_name = $form->name;
        $user->password = $form->password;

        if ($user->validate() && $user->save()) {
            $email = $user->email;
            $password = $form->password;

            $identity = new LocalUserIdentity($email, $password);

            if ($identity->authenticate()) {
                Yii::app()->user->login($identity);
            } else {
                $this->responseJson(['register' => 'error', 'errorMsg' => $identity->errorCode]);
            }

            $this->sendNotifyToUser($form);
            $this->sendNotifyToAdmin($user);

            $this->responseJson(['register' => 'success'], JSON_UNESCAPED_UNICODE);
        } else {
            $this->responseJson(['register' => 'error', 'errorMsg' => $this->getFirstError($user)]);
        }
    }

    public function actionLogin()
    {
        if (!isset($_POST['LoginForm']))
            $this->responseJson(['login' => 'error', 'error' => 'no data']);

        $identity = new LocalUserIdentity($_POST['LoginForm']['email'], $_POST['LoginForm']['password']);
        if (!$identity->authenticate()) {
            $identity->errorMessage = 'Username or password invalid';
            $this->responseJson(['login' => 'error', 'errorMsg' => $identity->errorMessage]);
        }

        Yii::app()->user->login($identity);
        $this->responseJson(['login' => 'success'], JSON_UNESCAPED_UNICODE);
    }

    public function actionReset()
    {
        if (!isset($_POST['ResetForm']['inputEmail']))
            $this->responseJson(['reset' => 'error', 'error' => 'Email was not set']);

        $user = User::model()->findByAttributes(['email' => $_POST['ResetForm']['inputEmail']]);
        if (!$user)
            $this->responseJson(['reset' => 'error', 'error' => 'User with this email not found']);

        $password = $user->generatePassword();
        $user->password = CPasswordHelper::hashPassword($password);
        $user->save();

        $this->sendResetPasswordNotify($user, $password);
        $this->responseJson(['reset' => 'success']);
    }

    private function getFirstError($model)
    {
        $errorMsg = '';
        foreach ($model as $key => $val) {
            if ($model->getError($key)) {
                $errorMsg .= $model->getError($key);
                break;
            }
        }

        return $errorMsg;
    }

    // TODO: move to Controller
    private function responseJson($data)
    {
        header('Content-type: application/json');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);

        Yii::app()->end();
    }

    //TODO: DRY
    private function sendNotifyToAdmin($model)
    {
        Yii::app()->mailer->IsMail();
        Yii::app()->mailer->From = 'site@' . $_SERVER['HTTP_HOST'];
        Yii::app()->mailer->FromName = 'LAWATO.COM';
        Yii::app()->mailer->AddAddress(Yii::app()->params['adminEmail']);
        Yii::app()->mailer->Subject = 'New user was registered';
        Yii::app()->mailer->MsgHTML('Name: ' . $model->full_name . '<br/>' . 'Email: ' . $model->email . '<br/>');
        Yii::app()->mailer->Send();
    }

    private function sendNotifyToUser($model)
    {
        Yii::app()->mailer->IsMail();
        Yii::app()->mailer->From = 'site@' . $_SERVER['HTTP_HOST'];
        Yii::app()->mailer->FromName = 'LAWATO.COM';
        Yii::app()->mailer->AddAddress($model->email);
        Yii::app()->mailer->Subject = 'Thank you for your registration';
        Yii::app()->mailer->MsgHTML('Name: ' . $model->name . '<br/>' . 'Email: ' . $model->email . '<br/>' . 'Password: ' . $model->password);
        Yii::app()->mailer->Send();
    }

    private function sendResetPasswordNotify($model, $password)
    {
        Yii::app()->mailer->IsMail();
        Yii::app()->mailer->From = 'site@' . $_SERVER['HTTP_HOST'];
        Yii::app()->mailer->FromName = 'LAWATO.COM';
        Yii::app()->mailer->AddAddress($model->email);
        Yii::app()->mailer->Subject = 'Password reset';
        Yii::app()->mailer->MsgHTML('New password for user ' . $model->email . ': ' . $password);
        Yii::app()->mailer->Send();
    }

    private function saveUploadedFile()
    {
        try {
            if (!isset($_FILES['upload']['error']) || is_array($_FILES['upload']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }

            switch ($_FILES['upload']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            if ($_FILES['upload']['size'] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            if (exif_imagetype($_FILES['upload']['tmp_name']) === false) {
                throw new RuntimeException('Invalid file format.');
            }

            $ext = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

            $this->uploadedFileName = sha1_file($_FILES['upload']['tmp_name']) . '.' . $ext;
            if (!move_uploaded_file($_FILES['upload']['tmp_name'],
                sprintf('./uploads/%s', $this->uploadedFileName))
            ) {
                throw new RuntimeException('Failed to move uploaded file.');
            }

            return true;
        } catch (RuntimeException $e) {
            $this->errorMsg = $e->getMessage();
            return false;
        }
    }
} 