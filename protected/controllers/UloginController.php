<?php

class UloginController extends Controller
{

    public function actionLogin() {

        if (isset($_POST['token'])) {

            $ulogin = new UloginModel();
            $ulogin->setAttributes($_POST);
            $ulogin->getAuthData();

            if ($ulogin->validate() && $ulogin->login()) {
                if(isset(Yii::app()->session['history']['address'])) {
                    $this->redirect(Yii::app()->session['history']['address']);
                    Yii::app()->end();
                }

                $this->redirect(Yii::app()->user->returnUrl);
            }
            else {
                $this->render('error');
            }
        }
        else {
            $this->redirect(Yii::app()->homeUrl, true);
        }
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        if(isset($_GET['redirect_to']))
            $this->redirect($_GET['redirect_to']);
        else
            $this->redirect(Yii::app()->homeUrl);
    }
}