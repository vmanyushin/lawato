<?php

/**
 * Created by PhpStorm.
 * User: swop
 * Date: 23.07.2015
 * Time: 7:36
 */
class CategoryController extends Controller
{
    public function init()
    {
        parent::init();

        $_SESSION['REFERER'] = $_SERVER['REQUEST_URI'];
    }

    public function actionIndex($state = '', $city = '', $category = '', $subcategory = '', $slug = '')
    {
        $params = compact('state', 'city', 'category', 'subcategory', 'slug');
        $meta   = [];
        $count  = '';

        $criteria = new CDbCriteria();

        if (Yii::app()->request->getParam('city', $city) > 0 &&
            Yii::app()->request->getParam('state', $state) == '') {
            $meta['state'] = $params['state'] = $state = City::model()->findByPk(Yii::app()->request->getParam('city', $city))->state_id;
        }

        if (Yii::app()->request->getParam('state', $state) > 0) {
            $criteria->condition = 'state_id = ' . Yii::app()->request->getParam('state', $state);
            $params['state'] = $meta['state'] = State::model()->findByPk(Yii::app()->request->getParam('state', $state));
        }

        if (Yii::app()->request->getParam('city', $city) > 0) {
            $criteria->condition = 'city_id  = ' . Yii::app()->request->getParam('city', $city);
            $params['city'] = $meta['city'] = City::model()->findByPk(Yii::app()->request->getParam('city', $city));
        }

        if(preg_match('/\/subcat-(\d+)/', $_SERVER['REQUEST_URI'], $matches)) {
            if($matches[1] > 0) {
                $params['category'] = $category = SubCategory::model()->findByPk($matches[1])->category_id;
            } else {
                throw new CHttpException(404,'page not found');
            }
        }

        if(Yii::app()->request->getParam('subcategory', $subcategory) > 0 &&
            empty($category)) {
            $params['category'] = $category = SubCategory::model()->findByPk(Yii::app()->request->getParam('subcategory', $subcategory))->category_id;
        }

        if (Yii::app()->request->getParam('category', $category) > 0) {
            $criteria->addCondition('category.id = ' . Yii::app()->request->getParam('category', $category));
            $criteria->together = true;
            $criteria->with = ['subCategories', 'subCategories.category'];

            $meta['category'] = Category::model()->findByPk(Yii::app()->request->getParam('category', $category));
            $count = 100000;
        }

        if (Yii::app()->request->getParam('subcategory', $subcategory) > 0) {
            $criteria->addCondition('subCategories.id = ' . Yii::app()->request->getParam('subcategory', $category));
            $meta['subcategory'] = SubCategory::model()->findByPk(Yii::app()->request->getParam('subcategory', $subcategory));
        }

        $criteria->limit = $this->pageSize;

        if($count == '') $count = Company::model()->count($criteria);

        if ($this->page > 0)
            $criteria->offset = ($this->page - 1) * $this->pageSize;
        else
            $criteria->offset = 0;

        $items = Company::model()->findAll($criteria);
        if(count($items) < 10) $count = count($items);

        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';

        $pages = new CPagination($count);
        $pages->pageSize = $this->pageSize;
        $pages->applyLimit($criteria);

        $this->render('index', [
            'params' => $params,
            'items'  => $items,
            'pages'  => $pages,
            'count'  => $count,
            'meta'   => $meta,
        ]);
    }
}