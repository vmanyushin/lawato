<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 20.08.2015
 * Time: 5:02
 */

class CompanyForm extends CFormModel
{
    public $id;
    public $name;
    public $state_id;
    public $city_id;
    public $user_id;
    public $zip_code;
    public $address;
    public $phones;
    public $fax;
    public $website;
    public $logo;
    public $images;
    public $personal;
    public $description;
    public $hours;
    public $video;
    public $status;
    public $created_at;
    public $categories;

    public function rules()
    {
        return array(
            array('name', 'length', 'min' => 3, 'max' => 255),
            array('id, status, state_id, city_id, user_id', 'numerical', 'integerOnly' => true),
            array('zip_code', 'length', 'max' => 15),
            array('address', 'required'),
            array('fax', 'length', 'max' => 15),
            array('address, website, video, logo', 'length', 'max'=>255),
            array('images, description, personal, phones, hours', 'safe'),
            array('created_at', 'required'),
        );
    }
} 