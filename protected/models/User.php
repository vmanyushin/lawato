<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $identity
 * @property string $network
 * @property string $uid
 * @property string $email
 * @property string $full_name
 * @property string $password
 * @property integer $state
 * @property string $logo
 * @property string $network_logo
 * @property string $gender
 * @property string $birthday
 * @property string $position
 * @property string $params
 * @property string $created_at
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_at', 'required'),
            array('identity, network, state, created_at', 'required', 'on' => 'ulogin'),
			array('state', 'numerical', 'integerOnly'=>true),
			array('identity, network, email, full_name, logo, network_logo, position', 'length', 'max'=>255),
			array('uid', 'length', 'max'=>45),
			array('password', 'length', 'max'=>100),
            array('password', 'compare', 'allowEmpty' => true, 'compareAttribute' => 'passwordRepeat', 'operator' => '=='),
			array('gender', 'length', 'max'=>1),
            array('birthday', 'date', 'format' => 'yyyy-MM-dd', 'except' => 'update-logo'),
            array('logo', 'required', 'on' => 'update-logo'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uid, email, full_name, logo, network_logo, gender, birthday, position, params, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'identity' => 'Identity',
			'network' => 'Network',
			'uid' => 'Uid',
			'email' => 'Email',
			'full_name' => 'Full Name',
			'password' => 'Password',
			'state' => 'State',
			'logo' => 'Logo',
			'network_logo' => 'Network Logo',
			'gender' => 'Gender',
			'birthday' => 'Birthday',
			'position' => 'Position',
			'params' => 'Params',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('identity',$this->identity,true);
		$criteria->compare('network',$this->network,true);
		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('state',$this->state);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('network_logo',$this->network_logo,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('params',$this->params,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Return if possible avatar from social network, if account register directly return or
     * user set his photo direct return local image
     */
    public function getPhoto()
    {
        $photo = '//ssl.gstatic.com/accounts/ui/avatar_2x.png';

        if (!empty($this->network) && empty($this->logo)) {
            $photo = $this->network_logo;
        } elseif(!empty($this->logo)) {
            $photo = '//' . $_SERVER['HTTP_HOST'] . '/uploads/' . $this->logo;
        }

        return $photo;
    }

    public function getPasswordRepeat()
    {
        return $this->password;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord) {
                $this->password = CPasswordHelper::hashPassword($this->password);
            }

            return true;
        }
        else
            return false;
    }

    public function isExist()
    {
        $user = $this->findByAttributes(['email' => $this->email]);
        if(isset($user->id))
            return $user->id;
        else
            return false;
    }

    public function generatePassword()
    {
        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
        return implode(array_slice($chars, 0, $length));
    }

    public function afterFind()
    {
        $this->birthday = strftime("%Y-%m-%d", strtotime($this->birthday));
    }

    public function getAge()
    {
        $birth   = new DateTime($this->birthday);
        $current = new DateTime();
        $diff    = $birth->diff($current);
        $ago     =  explode(',', $diff->format('%y,%m,%d,%h,%i,%s'));

        return $ago[0];
    }
}
