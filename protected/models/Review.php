<?php

/**
 * This is the model class for table "review".
 *
 * The followings are the available columns in table 'review':
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $grade
 * @property string $header
 * @property string $content
 * @property integer $approved
 * @property string $created_at
 * @property string $updated_at
 */
class Review extends CActiveRecord
{
    public function is_published()
    {
        return $this->approved == 1 ? true : false;
    }


    public function scopes()
    {
        return array(
            'approved' => array(
                'condition' => 'approved = 1',
            ),

            'all' => array(
                'condition' => 'approved in (1,0)',
            )
        );
    }

    public function withCompany($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => 'company_id = :company_id',
            'params' => [':company_id' => $id],
        ]);
        return $this;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, company_id, grade, content', 'required'),
			array('user_id, company_id, grade, approved', 'numerical', 'integerOnly'=>true),
			array('header', 'length', 'max'=>255),
			array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, company_id, grade, header, content, approved, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'company_id' => 'Company',
			'grade' => 'Grade',
			'header' => 'Header',
			'content' => 'Content',
			'approved' => 'Approved',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('grade',$this->grade);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('content',$this->content,true);
        $criteria->compare('approved',$this->approved);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Review the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
