<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 13.08.2015
 * Time: 16:51
 */

class UserForm extends CFormModel
{
    public $full_name;
    public $email;
    public $gender;
    public $birthday;
    public $password;
    public $passwordRepeat;
    public $params;
    public $logo;

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('password', 'length', 'max'=>100),
            array('gender', 'length', 'max'=>1),
            array('birthday', 'date', 'format' => 'yyyy-MM-dd'),
            array('password', 'length', 'max' => 100),
            array('password', 'length', 'max' => 100),
            array('passwordRepeat', 'comparePassword'),

            array('email', 'email', 'allowEmpty' => false ),
            array('email, full_name, gender, birthday', 'required'),

        );
    }

    public function comparePassword()
    {
        if($this->password == $this->passwordRepeat) {
            $this->password = CPasswordHelper::hashPassword($this->password);
            Yii::app()->user->setFlash('password', "Password update successfully");
        } else {
            $this->addError('passwordRepeat','pasword not match');
        }

        return true;
    }

    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
            'full_name' => 'Full Name',
            'password' => 'New Password',
            'passwordRepeat' => 'Repeat Password',
            'gender' => 'Gender',
            'birthday' => 'Birthday',
        );
    }
}