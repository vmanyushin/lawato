<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 18.08.2015
 * Time: 16:16
 */

class ReviewForm extends CFormModel
{
    public $grade;
    public $content;
    public $name;
    public $company_id;

    public function rules()
    {
        return array(
            array('grade, content', 'required'),
            array('grade', 'numerical', 'max' => 5, 'min' => 1),
            array('company_id', 'numerical', 'min' => 1),
            array('content', 'length', 'min' => 10),
        );
    }


} 