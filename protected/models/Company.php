<?php

/**
 * This is the model class for table "Company".
 *
 * The followings are the available columns in table 'Company':
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $user_id
 * @property integer $state_id
 * @property integer $city_id
 * @property string $address
 * @property string $zip_code
 * @property string $personal
 * @property string $phones
 * @property string $fax
 * @property string $website
 * @property string $hours
 * @property string $video
 * @property string $description
 * @property string $logo
 * @property string $images
 * @property string $categories
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property State $state
 * @property City $city
 * @property SubCategory[] $subCategories
 * @property Review[] $reviews
 */
class Company extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, state_id, city_id, created_at', 'required'),
			array('status, user_id, state_id, city_id', 'numerical', 'integerOnly'=>true),
			array('name, address, website, video, logo', 'length', 'max'=>255),
			array('zip_code', 'length', 'max'=>15),
			array('fax', 'length', 'max'=>45),
			array('personal, phones, description, images, hours', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, status, user_id, state_id, city_id, address, zip_code, personal, phones, fax, website, hours, video, description, logo, images, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'state' => array(self::BELONGS_TO, 'State', 'state_id'),
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'subCategories' => array(self::MANY_MANY, 'SubCategory', 'company_subcategory(company_id, subcategory_id)'),
            'review' => array(self::HAS_MANY, 'Review', 'company_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'status' => 'Status',
			'user_id' => 'User',
			'state_id' => 'State',
			'city_id' => 'City',
			'address' => 'Address',
			'zip_code' => 'Zip Code',
			'personal' => 'Personal',
			'phones' => 'Phones',
			'fax' => 'Fax',
			'website' => 'Website',
			'hours' => 'Hours',
			'video' => 'Video',
			'description' => 'Description',
			'logo' => 'Logo',
			'images' => 'Images',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('state_id',$this->state_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('zip_code',$this->zip_code,true);
		$criteria->compare('personal',$this->personal,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('hours',$this->hours,true);
		$criteria->compare('video',$this->video,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('images',$this->images,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function afterFind()
    {
        if (!empty($this->hours))    $this->hours    = new WorkHours($this->hours);
        if (!empty($this->phones))   $this->phones   = json_decode($this->phones);
        if (!empty($this->personal)) $this->personal = json_decode($this->personal);
        if (!empty($this->images))   $this->images   = json_decode($this->images);

        return true;
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {

            if(!$this->isNewRecord) {
                $this->updated_at = date('Y-m-d H:i:s');
            }

            if (isset($this->images)) {
                $this->images = json_encode($this->images);
            }

            if (isset($this->phones))   $this->phones   = json_encode($this->phones);
            if (isset($this->personal)) $this->personal = json_encode($this->personal);

            $this->user_id = Yii::app()->user->id;
            return true;

        } else {
            return false;
        }
    }

    public function getUrlPath()
    {
        return '/' . $this->id . '-' . Utils::slugify($this->name);
    }

    public function getFullAddress()
    {
        return $this->address . ', ' . $this->city->name . ', ' . $this->state->name;
    }


}
