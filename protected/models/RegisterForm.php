<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 02.08.2015
 * Time: 8:20
 */

class RegisterForm extends CFormModel
{
    public $name;
    public $email;
    public $password;
    public $passwordRepeat;

    public function rules()
    {
        return array(
            array('name, email, password, passwordRepeat', 'required'),
            array('email', 'email'),
            array('password', 'compare', 'allowEmpty' => false, 'compareAttribute' => 'passwordRepeat', 'operator' => '==')
        );
    }
}