<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 27.07.2015
 * Time: 14:31
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template text-center">
                <h1>
                    Oops!</h1>
                <h2>
                    <?=Yii::app()->errorHandler->error['code']?> <?=Yii::app()->errorHandler->error['message']?></h2>
                <div class="error-details">
                    Sorry, an error has occured
                </div>
                <div class="error-actions">
                    <a href="/" class="btn btn-primary"><span class="glyphicon glyphicon-home"></span>&nbsp; Take Me Home</a>

                </div>
            </div>
        </div>
    </div>
</div>


