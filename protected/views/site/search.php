<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 01.08.2015
 * Time: 20:40
 */
?>
<? $h1 = 'Search result for: ' . Yii::app()->session['search']['name'] . ' at ' . Yii::app()->session['search']['location'] ?>

<? $this->renderPartial('//shared/menu', ['params' => [], 'displayFilter' => false]); ?>

<div class="container-fluid with-sidebar">
    <div class="row">
        <div class="col-lg-12">
            <h1><?= $h1 ?></h1>
        </div>
    </div>

    <div class="row">
        <? $this->renderPartial('//shared/company', ['items' => $items]); ?>
    </div>

    <? $this->renderPartial('//shared/pagination', ['pages' => $pages]); ?>
</div>