<?php
Yii::app()->clientScript->registerMetaTag('Find a local lawyer and free legal information at LAWATO, the award-winning website.', 'description');
$this->page_title = 'LAWATO, Legal Information, and Attorneys - LAWATO'
?>

<div class="container-fluid">
    <div class="row with-background">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="/css/images/logo-big.png" alt="logotype">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 text-center no-padding">
                    <p class="big-36">You have already found a lawyer?</p>
                </div>
            </div>

            <? $this->renderPartial('//shared/search'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Browse by Common Legal Issues</h2>
        </div>

        <div class="col-lg-12">
            <div class="row category-row">
                <?php foreach ($categories as $category): ?>
                    <div class="col-lg-3 col-md-4 col-sm-6"><a
                            href="/cat-<?= $category->id . '-' . Utils::slugify($category->name) ?>"
                            class="btn btn-category"><?= $category->name ?></a></div>
                <? endforeach ?>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Browse Lawyers by State</h2>
        </div>

        <div class="col-lg-12">
            <div class="row state-row">
                <?php foreach ($states as $state): ?>
                    <div class="col-lg-2 col-md-3 col-sm-4"><a
                            href="/<?= strtolower($state->abbreviation) . '-' . $state->id . '-' . Utils::slugify($state->name) ?>"
                            class="btn btn-state"><?= $state->name ?></a></div>
                <? endforeach ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Lawyers in Top Cities</h2>
        </div>

        <div class="col-lg-12">
            <div class="row city-row">
                <?php foreach ($cities as $city): ?>
                    <div class="col-lg-2 col-md-3 col-sm-4"><a
                            href="/city-<?= $city->id . '-' . Utils::slugify($city->name) ?>"
                            class="btn btn-city"><?= $city->name ?></a></div>
                <? endforeach ?>
            </div>
        </div>
    </div>
</div>

