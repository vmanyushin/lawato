<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 27.08.2015
 * Time: 17:47
 */
?>


<?php
$hours = [
    '0' => '12:00 AM',
    '1' => '1:00 AM',
    '2' => '2:00 AM',
    '3' => '3:00 AM',
    '4' => '4:00 AM',
    '5' => '5:00 AM',
    '6' => '6:00 AM',
    '7' => '7:00 AM',
    '8' => '8:00 AM',
    '9' => '9:00 AM',
    '10' => '10:00 AM',
    '11' => '11:00 AM',
    '12' => '12:00 PM',
    '13' => '1:00 PM',
    '14' => '2:00 PM',
    '15' => '3:00 PM',
    '16' => '4:00 PM',
    '17' => '5:00 PM',
    '18' => '6:00 PM',
    '19' => '7:00 PM',
    '20' => '8:00 PM',
    '21' => '9:00 PM',
    '22' => '10:00 PM',
    '23' => '11:00 PM',
]
?>

<div class="panel panel-lawato">
    <div class="panel-heading">Working hours</div>
    <div class="panel-body">
        <table class="table-condensed work-hours">
            <thead>
            <th>Weekday</th>
            <th>From</th>
            <th>To</th>
            </thead>
            <?php foreach (['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] as $weekday): ?>
                <tbody>
                <tr>
                    <td><label for="CompanyForm[hours][<?= $weekday ?>]"></label><?= $weekday ?></td>
                    <td>
                        <select name="CompanyForm[hours][<?= $weekday ?>_from]" class="form-control">
                            <option value="">Closed</option>
                            <?php foreach ($hours as $key => $time): ?>
                                <option value="<?= $key ?>" <?= $model->hours->isSelected($weekday . '_from', $key) ?>><?= $time ?></option>
                            <?php endforeach ?>
                        </select>
                    </td>

                    <td>
                        <select name="CompanyForm[hours][<?= $weekday ?>_to]" class="form-control">
                            <option value="">Closed</option>
                            <?php foreach ($hours as $key => $time): ?>
                                <option value="<?= $key ?>" <?= $model->hours->isSelected($weekday . '_to', $key) ?>><?= $time ?></option>
                            <?php endforeach ?>
                        </select>
                    </td>
                </tr>
                </tbody>
            <?php endforeach ?>
        </table>
    </div>
</div>
