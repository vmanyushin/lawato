<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 20.08.2015
 * Time: 5:07
 */
?>

<? $this->renderPartial('//shared/menu'); ?>

<div class="container-fluid with-sidebar">
    <div class="row">
        <div class="col-lg-12">
            <h1>My companies</h1><a class="btn btn-default btn-lawato-blue add-new-company" href="/company/create"><span class="glyphicon glyphicon-plus"></span> Add new
                company
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover table-responsive">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>State</th>
                        <th>City</th>
                        <th>Address</th>
                        <th>Zip Code</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach($model as $company): ?>
                    <tr>
                        <td><a href="<?= $company->urlpath ?>" target="_blank"><?= $company->name ?></a></td>
                        <td><?= $company->state->name ?></td>
                        <td><?= $company->city->name ?></td>
                        <td><?= $company->address ?></td>
                        <td><?= $company->zip_code ?></td>
                        <td>
                            <a href="/company/edit/<?=$company->id?>" class=""><span class="glyphicon glyphicon-pencil"></span> edit</a>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>

        </div>
    </div>
</div>