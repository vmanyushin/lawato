<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 22.08.2015
 * Time: 8:56
 */
?>


<? $this->renderPartial('//shared/menu'); ?>

<div class="container-fluid with-sidebar edit-company">
<div class="row">
    <div class="col-lg-12">
        <h1>Edit info for <?= $model->name ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php $this->renderPartial('_images', ['model' => $model]); ?>
    </div>
</div>

<?php $form = $this->beginWidget('CActiveForm', [
    'id' => 'edit-form',
    'focus' => [$model, 'name'],
    'enableClientValidation' => true,
    'htmlOptions' => [
        'class' => 'form',
    ]
]); ?>

<div class="panel panel-lawato">
    <div class="panel-heading">Company name</div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?= $form->textField($model, 'name', ['class' => 'form-control']); ?>
                    <?= $form->error($model, 'name'); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="panel panel-lawato">
    <div class="panel-heading">Address information</div>
    <div class="panel-body">
        <div class="row">
            <div class="form-group">
                <div class="col-lg-4">
                    <?php echo CHtml::label('State', $model->state_id); ?>
                    <?= CHtml::activeDropDownList($model, 'state_id',
                        CHtml::listData(State::model()->findAll(), 'id', 'name'),
                        array(
                            'prompt' => 'State',
                            'ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->createUrl('company/loadcities'),
                                'update' => '#CompanyForm_city_id',
                                'data' => array('id' => 'js:this.value'),
                            ),
                            'required' => 'required',
                            'class' => 'form-control',
                        )
                    ); ?>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <?php echo CHtml::label('City', $model->city_id); ?>
                        <?= CHtml::activeDropDownList($model, 'city_id',
                            CHtml::listData(City::model()->findAll(['condition' => 'state_id = :state_id', 'params' => [':state_id' => $model->state_id]]), 'id', 'name'),
                            array(
                                'prompt' => 'City',
                                'required' => 'required',
                                'class' => 'form-control',
                            )
                        ); ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <?php echo CHtml::label('Zip Code', 'CompanyForm[zip_code]'); ?>
                        <?php echo CHtml::activeTextField($model, 'zip_code', ['class' => 'form-control', 'required' => 'required', 'minlength' => 5, 'maxlength' => 15, 'placeholder' => '12345']) ?>
                        <?= $form->error($model, 'zip_code'); ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <?php echo CHtml::label('Street Address', $model->address); ?>
                <?php echo CHtml::activeTextField($model, 'address', ['class' => 'form-control', 'required' => 'required']) ?>
                <?= $form->error($model, 'address'); ?>
            </div>
        </div>

    </div>

</div>

<div class="panel panel-lawato">
    <div class="panel-heading">Contacts</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-4">
                <label for="phones[]">Phones</label>
                <table id="phones" class="table-wide">
                    <?php foreach ($model->phones as $phone): ?>
                        <tr>
                            <td>
                                <input type="text" name="CompanyForm[phones][]" value="<?= $phone ?>"
                                       class="form-control"/>
                                <a href="javascript:;" class="delete-phone"><span>&times;</span></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <td><?= $form->error($model, 'phones'); ?></td>
                    </tr>
                </table>
                <a href="javascript:;" class="btn btn-default btn-lawato-blue" id="add-phone">+ Add more phones</a>
            </div>

            <div class="col-lg-4">
                <?php echo CHtml::label('Fax', $model->fax); ?>
                <?php echo CHtml::activeTextField($model, 'fax', ['class' => 'form-control']) ?>
                <?= $form->error($model, 'fax'); ?>
            </div>

            <div class="col-lg-4">
                <?php echo CHtml::label('Website', $model->website); ?>
                <?php echo CHtml::activeUrlField($model, 'website', ['class' => 'form-control']) ?>
                <?= $form->error($model, 'website'); ?>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-lawato">
    <div class="panel-heading">Personal</div>
    <div class="panel-body">
        <table class="table table-wide" id="personal">
            <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th></th>
            </tr>
            </thead>
            <?php if(!is_array($model->personal)) {
                $model->personal = json_decode($model->personal);
            }?>
            <?php foreach ($model->personal as $index => $person): ?>
                <tr>
                    <td>
                        <input type="hidden" name="CompanyForm[personal][<?= $index ?>][url]"
                               value="<?= $person[0] ?>"/>
                        <input type="text" name="CompanyForm[personal][<?= $index ?>][name]"
                               value="<?= $person[1] ?>" class="form-control"/>
                    </td>
                    <td><input type="text" name="CompanyForm[personal][<?= $index ?>][position]"
                               value="<?= $person[2] ?>" class="form-control"/></td>
                    <td><a href="javascript:;" class="btn btn-default btn-lawato-blue delete-personal" role="button">delete</a></td>
                </tr>
            <?php endforeach ?>
        </table>
        <a href="javascript:;" class="btn btn-default btn-lawato-blue" id="add-personal">+ Add more person</a>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <?php $this->renderPartial('_categories', ['model' => $model]); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php $this->renderPartial('_workhours', ['model' => $model]); ?>
    </div>
</div>

<div class="panel panel-lawato">
    <div class="panel-heading">Description</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php echo CHtml::activeTextArea($model, 'description', ['class' => 'form-control', 'rows' => 10]) ?>
                <?= $form->error($model, 'description'); ?>
            </div>
        </div>
    </div>
</div>

<p></p>

<?php echo CHtml::activeHiddenField($model, 'created_at', ['class' => 'form-control']) ?>
<?= CHtml::submitButton('Submit', ['class' => 'btn btn-default btn-lawato-blue']) ?>

<?php $this->endWidget() ?>
</div>

<script src="/js/jquery.ajaxfileupload.js"></script>
<script>
    $('label.add-logo').click(function (e) {
        $('#upload-logo').click()
    });

    $('#upload-logo').ajaxfileupload({
        'action': '/company/uploadcompanylogo/' + '<?= $model->id ?>',
        'onComplete': function (response) {
            if (response.status == 'success') {
                $('.logo-container > img').prop('src', response.path).show().next().hide().parent().find('a').show();
            }
            else {
                console.log(response.reason);
            }
        }
    });

    $('#upload-image').ajaxfileupload({
        'action': '/company/uploadcompanyimage/' + '<?= $model->id ?>',
        'onComplete': function (response) {
            if (response.status == 'success' && response.path.length > 0) {
                for (var i = 0; i < response.path.length; i++) {
                    $('.image-container:eq(' + i + ')').find('img').prop('src', response.path[i]).show().next().hide().parent().find('a').show();
                }
            } else {
                console.log(response.reason);
            }
        }
    });

    $('label.add-image').click(function () {
        $('#upload-image').click()
    })

    $('.delete-logo').click(function (e) {
        $.ajax({
            url: '/company/deletecompanylogo/' + '<?= $model->id ?>',
            dataType: 'json',
            success: function (data) {
                if (data.status == 'success') {
                    $('.logo-container > img').hide().next().show().parent().find('a').hide();
                } else {
                    console.log(data);
                }
            }
        });
    });

    $('.delete-image').click(function (e) {
        var index = $(e.target).parent().data('index');
        console.log(index);
        $.ajax({
            url: '/company/deletecompanyimage/' + '<?= $model->id ?>' + '/' + index,
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    $('.image-container').each(function (i, e) {
                        $(e).find('img').prop('src', '').hide();
                    });

                    for (var i = 0; i < 5; i++) {
                        if (i < response.path.length) {
                            $('.image-container:eq(' + i + ')').find('img').prop('src', response.path[i]).show().prev().show();
                        } else {
                            console.log('clear: ' + i);
                            $('.image-container:eq(' + i + ')').find('img').prop('src', '').hide().next().show().parent().find('a').hide();
                        }
                    }

                } else {
                    console.log(response.status);
                }
            }
        });
    });

    $("#add-phone").click(function () {
        $("#phones").append('<tr><td><input type="text" name="CompanyForm[phones][]" value="" class="form-control"/></td></tr>');
    });

    $(".delete-phone").click(function (e) {
        $(e.target).parent().parent().remove();
    });

    $("#add-personal").click(function () {
        var lastid = $("#personal tbody tr").last().find('td').first().find('input').prop('name').match(/CompanyForm.*(\d+)/)[1];

        if(lastid == undefined)
            lastid = 0;
        else
            lastid++

        $("#personal tbody").append('<tr><td><input type="hidden" name="CompanyForm[personal][' + lastid + '][url]" value="" class="form-control"/><input type="text" name="CompanyForm[personal][' + lastid + '][name]" value="" class="form-control"/></td><td><input type="text" name="CompanyForm[personal][' + lastid + '][position]" value="" class="form-control"/></td><td><a href="javascript:;" class="btn btn-default btn-lawato-blue" role="button" id="delete-personal">delete</a></td></tr>');
    });

    $('.delete-personal').click(function(e){$(e.target).parent().parent().remove();})
</script>