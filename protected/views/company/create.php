<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 22.08.2015
 * Time: 8:55
 */
?>


<? $this->renderPartial('//shared/menu'); ?>

<div class="container-fluid with-sidebar">
    <div class="row">
        <div class="col-lg-12">
            <h1>Create new company</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <?php $activeForm = $this->beginWidget('CActiveForm', array(
                'action' => Yii::app()->createUrl($this->route),
                'method' => 'post',
                'htmlOptions' => array(
                    'class' => 'form-group create-company',
                ),
            )); ?>

            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <?php echo CHtml::label('Company name', $form->name); ?>
                        <?php echo CHtml::activeTextField($form, 'name', [
                            'class' => 'form-control',
                            'required' => 'required',
                        ]);
                        ?>
                        <?= $activeForm->error($form, 'name'); ?>
                    </div>
                </div>
            </div>


            <h2>Address information</h2>

            <div class="row">
                <div class="form-group">
                    <div class="col-lg-4">
                        <?php echo CHtml::label('State', $form->state_id); ?>
                        <?= CHtml::activeDropDownList($form, 'state_id',
                            CHtml::listData(State::model()->findAll(), 'id', 'name'),
                            array(
                                'prompt' => 'State',
                                'ajax' => array(
                                    'type' => 'POST',
                                    'url' => Yii::app()->createUrl('company/loadcities'),
                                    'update' => '#CompanyForm_city_id',
                                    'data' => array('id' => 'js:this.value'),
                                ),
                                'required' => 'required',
                                'class' => 'form-control',
                            )
                        ); ?>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-4">
                            <?php echo CHtml::label('City', $form->city_id); ?>
                            <?= CHtml::activeDropDownList($form, 'city_id',
                                CHtml::listData(City::model()->findAll(['condition' => 'state_id = :state_id', 'params' => [':state_id' => $form->state_id]]), 'id', 'name'),
                                array(
                                    'prompt' => 'City',
                                    'required' => 'required',
                                    'class' => 'form-control',
                                )
                            ); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-4">
                            <?php echo CHtml::label('Zip Code', 'CompanyForm[zip_code]'); ?>
                            <?php echo CHtml::activeNumberField($form, 'zip_code', ['class' => 'form-control', 'required' => 'required', 'minlength' => 5, 'maxlength' => 5, 'placeholder' => '12345']) ?>
                            <?= $activeForm->error($form, 'zip_code'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <?php echo CHtml::label('Street Address', $form->address); ?>
                    <?php echo CHtml::activeTextField($form, 'address', ['class' => 'form-control', 'required' => 'required']) ?>
                    <?= $activeForm->error($form, 'address'); ?>
                </div>
            </div>

            <h2>Contacts</h2>

            <div class="row">
                <div class="col-lg-4">
                    <?php echo CHtml::label('Phones', $form->phones); ?>
                    <?php echo CHtml::activeTextField($form, 'phones', ['class' => 'form-control']) ?>
                    <?= $activeForm->error($form, 'phones'); ?>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-4">
                    <?php echo CHtml::label('Fax', $form->fax); ?>
                    <?php echo CHtml::activeTextField($form, 'fax', ['class' => 'form-control']) ?>
                    <?= $activeForm->error($form, 'fax'); ?>
                </div>

            </div>

            <?= CHtml::submitButton('Submit', ['class' => 'btn btn-default btn-lawato-blue']) ?>

            <?php $this->endWidget() ?>

        </div>
    </div>
</div>

<?php Yii::app()->getClientScript()->registerScriptFile('/js/jquery.maskedinput.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScript('phonemask',"
    $('#CompanyForm_phones, #CompanyForm_fax').mask('(999) 999-9999', {'placeholder': '(111) 111-1111'});
", CClientScript::POS_END);
?>