<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 27.08.2015
 * Time: 22:22
 */
?>

<?php
    $counter = array_fill(0, 19, 0);

    if(is_array($model->categories)) {
        foreach($model->categories as $id) {
            $counter[SubCategory::model()->findByPk($id)->category->id]++;
        }
    }
?>

<div class="panel panel-lawato">
    <div class="panel-heading">Select categories</div>
    <div class="panel-body">
        <div class="col-lg-4">
            <?php foreach (Category::model()->findAll() as $category): ?>
                <div class="">
                    <a class="" role="button" data-toggle="collapse" href="#<?= Utils::slugify($category->name) ?>">
                        <?= $category->name ?> <span class="badge"><?= $counter[$category->id] ?></span>
                    </a>
                </div>


                <div class="collapse" id="<?= Utils::slugify($category->name) ?>">
                    <?php foreach (SubCategory::model()->findAllByAttributes(['category_id' => $category->id]) as $subCategory): ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="CompanyForm[categories][]" value="<?= $subCategory->id ?>" <?php if(in_array($subCategory->id, $model->categories)) echo 'checked'?>/>
                            <?= $subCategory->name ?>
                        </label>
                    </div>
                    <?php endforeach ?>
                </div>

            <?php endforeach ?>
        </div>
    </div>
</div>

<script>
    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);
</script>