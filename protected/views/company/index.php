<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 25.07.2015
 * Time: 1:55
 */

$category = [];
$specialized = '';
$counter = 5;

foreach ($item->subCategories as $sub_category) {
    $category[$sub_category->category->name]['path'] = '/cat-' . $sub_category->category->id . '-' . Utils::slugify($sub_category->category->name);
    $category[$sub_category->category->name]['subc'][] = ['/subcat-' . $sub_category->id . '-' . Utils::slugify($sub_category->name), $sub_category->name];

    if (--$counter > 0) $specialized[] = $sub_category->name;
}

$this->page_title = $item->name . ' - an ' . $item->city->name . ', ' . $item->state->name . ' (' . $item->state->abbreviation . ') LAWATO';
Yii::app()->clientScript->registerMetaTag('Find ' . $item->name . ' an ' . $item->city->name . ', ' . $item->state->name .
    ' (' . $item->state->abbreviation . ') Law Firm focused on ' . implode(', ', $specialized) . ' etc.', 'description');
?>

<? $this->renderPartial('//shared/menu', ['params' => $params]); ?>

    <div class="container-fluid with-sidebar">
    <div class="col-lg-12 col-md-12" itemtype="http://schema.org/Attorney" itemscope="true">
    <div class="row">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- lawato-320-100-at-top -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:320px;height:100px"
             data-ad-client="ca-pub-1403577915272443"
             data-ad-slot="1710772865"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>


        <h1><?= $item->name ?>, <?= $item->city->name ?>, <?= $item->state->abbreviation ?></h1>

        <?php if(Yii::app()->user->isGuest == true): ?>
        <a class="btn btn-default btn-lawato-blue red-tooltip" href="javascript:;" data-placement="bottom" title="Authentication required, please login or signup" data-toggle="tooltip" id="quick-edit-btn"><span
                class="glyphicon glyphicon-edit" rel="nofollow">&nbsp;</span>Quick edit</a>
        <a class="btn btn-default btn-lawato-blue red-tooltip" href="javascript:;" rel="nofollow" href="javascript:;" data-placement="bottom" title="Authentication required, please login or signup" data-toggle="tooltip"><span
                class="glyphicon glyphicon-comment">&nbsp;</span>Write
            review</a>
        <?php else: ?>
            <a class="btn btn-default btn-lawato-blue" href="/company/edit/<?= $item->id ?>"<span class="glyphicon glyphicon-edit" rel="nofollow">&nbsp;</span>Quick edit</a>
            <a class="btn btn-default btn-lawato-blue" href="#review" rel="nofollow"><span class="glyphicon glyphicon-comment">&nbsp;</span>Write review</a>
        <?php endif ?>
        <hr/>
    </div>

    <div class="row">
        <div class="pull-left">
            <?php if (empty($item->logo) || !file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/logo/' . $item->logo)): ?>
                <img src="/css/images/nologo.png"
                     alt="<?= $item->name ?>"
                     class="img-thumbnail img-logo height-auto"/>
            <? else: ?>
                <img src="/images/logo/<?= $item->logo ?>"
                     alt="<?= $item->name ?>"
                     class="img-thumbnail img-logo height-auto"/>
            <? endif ?>
        </div>

        <div itemtype="http://schema.org/PostalAddress" itemscope="true" itemprop="address"
             class="company-card pull-left">
            <p>
                <span itemprop="streetAddress"><?= $item->address ?></span>,
                <span itemprop="addressLocality"><?= $item->city->name ?></span>,
                <span itemprop="addressRegion"><?= $item->state->abbreviation ?></span>
                <span itemprop="postalCode"><?= $item->zip_code ?></span>
            </p>

            <div class="rating clearfix" itemscope="" itemtype="http://schema.org/AggregateRating">
                <?php
                if(!isset($votes)) {
                    $this->widget('CStarRating', array(
                        'minRating' => 1,
                        'maxRating' => 5,
                        'starCount' => 5,
                        'value' => 0,
                        'name' => 'ur_' . 1,
                        'readOnly' => true
                    ));
                } else {
                    $this->widget('CStarRating', array(
                        'minRating' => 1,
                        'maxRating' => 5,
                        'starCount' => 5,
                        'value' => ($votes->sum / $votes->num),
                        'name' => 'ur_' . 1,
                        'readOnly' => true
                    ));
                }

                ?>
            </div>

            <p class="rating">
                <?php if(isset($votes->num)): ?>
                <span itemprop="ratingValue"><?= ($votes->sum / $votes->num) ?></span>/5 (<span itemprop="ratingCount"><?= $votes->num ?></span> votes)
                <?php else: ?>
                <span itemprop="ratingValue"></span>0/5 (<span itemprop="ratingCount">0</span> votes)
                <?php endif ?>
            </p>

            <p>
                <?php if (count($item->phones)): ?>
                    <?php foreach ($item->phones as $phone): ?>
                        <span itemprop="telephone" class="phone"><?= $phone ?> </span><br/>
                    <? endforeach; ?>
                <? endif; ?>
            </p>

            <?php if (!empty($item->fax)): ?>
                <p><span class="address-field">Fax: </span><span
                        itemprop="faxNumber"><?= $item->fax ?></span>
                </p>
            <? endif; ?>
        </div>

        <?php if ($item->hours): ?>
            <div class="office-hours pull-left">
                <h4>Office hours</h4>
                <?php if ($item->hours->format == 0): ?>
                    <p><?= $item->hours ?></p>
                <?php else: ?>
                    <table class="hours">
                        <?php foreach ($item->hours->weekdays as $day): ?>
                            <tr>
                                <td><b><?= substr($day, 0, 3) ?></b></td>
                                <?php if ($item->hours->from($day) == 'closed' && $item->hours->to($day) == 'closed'): ?>
                                    <td colspan="2">closed</td>
                                <?php else: ?>
                                    <td><?= $item->hours->from($day) ?></td>
                                    <td>- <?= $item->hours->to($day) ?></td>
                                <?php endif ?>
                            </tr>
                        <?php endforeach ?>
                    </table>
                <? endif ?>
            </div>
        <?php endif ?>
    </div>

    <?php if (count($item->images) > 0 || $item->video): ?>
        <div class="row">
            <div class="gallery" id="gallery">
                <?php foreach ($item->images as $image): ?>
                    <a href="//<?php echo $_SERVER['HTTP_HOST'] . Yii::app()->params['uploadPath'] . $image ?>"
                       data-gallery>
                        <img src="//<?php echo $_SERVER['HTTP_HOST'] . Yii::app()->params['uploadPath'] . $image ?>"
                             alt="<?= $item->name ?>" class="img-thumbnail"/>
                    </a>
                <?php endforeach ?>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="row">
                <h2><?= $item->name ?> OVERVIEW</h2>
            </div>

            <div class="row">
                <p><?= $item->description ?></p>
            </div>

            <div class="row practice-area">
                <h3>Practice Areas</h3>
                <?php foreach ($category as $key => $val): ?>
                    <div class="col-lg-2 col-md-3">
                        <h4><a href="<?= $category[$key]['path'] ?>"><?= $key ?></a></h4>
                        <ul class="list-unstyled">
                            <?php foreach ($category[$key]['subc'] as $subitem): ?>
                                <li><a href="<?= $subitem[0] ?>"><?= $subitem[1] ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>

    <?php if (count($item->personal) > 0): ?>
        <div class="row">
            <div class="" itemscope="true" itemtype="http://schema.org/Organization">
                <h3>PEOPLES WHO WORK HERE</h3>
                <ul class="list-unstyled">
                    <?php if(!is_array($item->personal)) $item->personal = json_decode($item->personal) ?>
                    <?php foreach ($item->personal as $person): ?>
                        <?php if ($person == null) continue ?>
                        <?php if (is_array($person)): ?>
                            <li><span class="member"
                                      itemprop="member"><?= $person[1] ?> <?php if (isset($person[2]) && !empty($person[2])) echo '(' . $person[2] . ')' ?></span>
                            </li>
                        <?php else: ?>
                        <li><span class="member"
                                  itemprop="member"><?= $person->Name ?> <?php if (isset($person->Position) && !empty($person->Position)) echo '(' . $person->Position . ')' ?></span>
                        </li>
                        <?php endif ?>
                    <? endforeach ?>
                </ul>
            </div>
        </div>
        <hr/>
    <?php endif ?>

    <div class="row">
        <div style="position: relative;">
            <div class="map-container">
                <a href="javascript:;" class="btn btn-map" id="open-map">Build route</a>

                <div class="direction-panel" id="direction-panel">
                    <button type="button" class="close" id="close-map">&times;</button>

                    <div class="form-inline">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">A</div>
                                <input type="text" class="form-control" id="from" placeholder="from address"
                                       value="new york">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">B</div>
                                <input type="text" class="form-control" id="to" placeholder="to address"
                                       value="<?= $item->fulladdress ?>">
                            </div>
                        </div>

                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lawato-blue" id="driving"
                                data-direction="DRIVING"><i class="car"></i></button>
                        <button type="button" class="btn btn-default btn-lawato-blue" id="transit"
                                data-direction="TRANSIT"><i class="bus"></i></button>
                        <button type="button" class="btn btn-default btn-lawato-blue" id="walking"
                                data-direction="WALKING"><i class="walk"></i></button>
                    </div>

                    <a href="javascript:;" class="btn btn-default btn-lawato-blue build-direction" id="build-direction">Build
                        directions</a>

                    <div id="panel-container" class="nano">
                        <div id="panel" class="nano-content"></div>
                    </div>

                </div>

                <div id="map-canvas" class="google-map"></div>
            </div>
        </div>
    </div>

    <hr/>

    <div class="row">
        <h3>Other Lawyers practicing near <?= $item->name ?></h3>
    </div>

    <div class="row">
        <? $this->renderPartial('//shared/company',
            ['items' => Company::model()->findAll([
                'condition' => "city_id = :city_id and state_id = :state_id",
                'limit' => 2,
                'params' => [
                    ':city_id' => $item->city_id,
                    ':state_id' => $item->state_id
                ]
            ])]); ?>
    </div>

    <?php if ($reviews): ?>
        <div class="row">
            <h2>Review</h2>

            <div class="col-lg-12 col-md-12">
                <? foreach ($reviews as $review): ?>
                    <?php $this->renderPartial('//shared/review', ['review' => $review]) ?>
                <? endforeach ?>
            </div>
        </div>
    <?php endif ?>

    <div class="row" id="review">
        <form action="/review" class="form" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="rating">
                        <?php $this->widget('CStarRating', array(
                            'model' => new ReviewForm(),
                            'name' => 'ReviewForm[grade]',
                            'minRating' => 1,
                            'maxRating' => 5,
                            'starCount' => 5,
                            'readOnly' => false
                        )); ?>
                    </div>
                </div>

                <div class="panel-body">
                    <textarea name="ReviewForm[content]" id="" cols="30" rows="10" maxlength="4000"
                              required></textarea>
                </div>
            </div>
            <input type="hidden" name="ReviewForm[company_id]" value="<?= $item->id ?>"/>
            <?php if(Yii::app()->user->isGuest == true): ?>
            <input type="button" value="Write review" class="btn btn-primary btn-lawato-blue" data-placement="bottom" title="Authentication required, please login or signup" data-toggle="tooltip"/>
            <? else: ?>
            <input type="submit" value="Write review" class="btn btn-primary btn-lawato-blue"/>
            <?php endif ?>
        </form>
    </div>

    <div class="spacer"></div>

    </div>
    </div>
<?php Yii::app()->getClientScript()->registerScript('address', "
    var address = '$item->fulladdress';
    var title   = '$item->name';
", CClientScript::POS_END); ?>

<?php Yii::app()->getClientScript()->registerScriptFile('https://maps.googleapis.com/maps/api/js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/gmaps.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/map.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/jquery.nanoscroller.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerCssFile('/css/nano.css', 'all', CClientScript::POS_END); ?>

<?php Yii::app()->getClientScript()->registerCssFile('/css/blueimp/blueimp-gallery.min.css', 'all', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerCssFile('/css/blueimp/blueimp-gallery-indicator.min.css', 'all', CClientScript::POS_END); ?>

<?php Yii::app()->getClientScript()->registerScriptFile('/js/blueimp/blueimp-gallery.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/blueimp/blueimp-gallery-indicator.min.js', CClientScript::POS_END); ?>

<?php Yii::app()->getClientScript()->registerScript("blueimp-gallery", "
    $('body').append('<div id=\"blueimp-gallery\" class=\"blueimp-gallery blueimp-gallery-controls\"><div class=\"slides\"></div><h3 class=\"title\"></h3><a class=\"prev\">‹</a><a class=\"next\">›</a><a class=\"close\">×</a></div>');
", CClientScript::POS_END); ?>


<?php Yii::app()->getClientScript()->registerScript("blueimp-gallery-handler", "
    if (document.getElementById('gallery')) {
        document.getElementById('gallery').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = {index: link, event: event},
                links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    }

    $(function () {
        $('[data-toggle=\"tooltip\"]').tooltip()
    })
", CClientScript::POS_END); ?>
