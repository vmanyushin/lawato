<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 27.08.2015
 * Time: 17:29
 */
?>

<div class="panel panel-lawato">
    <div class="panel-heading">
        Add logo and images
    </div>

    <div class="panel-body">
        <div class="logo-container">
            <?php if ($model->logo): ?>
                <a href="javascript:;" class="delete-logo"><span class="glyphicon glyphicon-remove"></span></a>
                <img src="<?= Yii::app()->params['logoPath'] . $model->logo ?>" alt="<?= $model->name ?>"
                     class="add-logo" width="96"/>
                <label class="add-logo" style="display: none">+ Add Logo</label>
            <?php else: ?>
                <a href="javascript:;" class="delete-logo" style="display: none"><span
                        class="glyphicon glyphicon-remove"></span></a>
                <img src="" alt="<?= $model->name ?>" class="add-logo" width="96"
                     style="display: none"/>
                <label class="add-logo">+ Add Logo</label>
            <?php endif ?>

            <form enctype="multipart/form-data" style="display: none;">
                <input type="file" name="upload-logo" id="upload-logo"/>
            </form>
        </div>

        <?php foreach (range(0, 4) as $index): ?>
            <div class="image-container">
                <?php if (!empty($model->images[$index])): ?>
                    <a href="javascript:;" class="delete-image" data-index="<?= $index ?>"><span
                            class="glyphicon glyphicon-remove"></span></a>
                    <img src="<?= Yii::app()->params['uploadPath'] . $model->images[$index] ?>"
                         alt="<?= $model->name ?>"
                         class="add-image" width="96"/>
                    <label class="add-image" style="display: none">+ Add Image</label>
                <?php else: ?>
                    <a href="javascript:;" class="delete-image" style="display: none"
                       data-index="<?= $index ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    <img src="" alt="<?= $model->name ?>" class="add-image" width="96" style="display: none"/>
                    <label class="add-image">+ Add Image</label>
                <?php endif ?>
            </div>
        <?php endforeach ?>

        <form enctype="multipart/form-data" style="display: none;">
            <input type="file" name="upload-image[]" id="upload-image" multiple/>
        </form>
    </div>
</div>
