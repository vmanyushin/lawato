<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <title>Панель администратора</title>
    </head>
    <body>
        <div class="container">
            <h1>Панель администратора</h1>
            <div class="row">
                <div class="span3">
                    <div class="well">
                        <ul class="nav nav-list">
                            <li id="m_default">
                                <a href="/admin">Главная</a>        
                            </li>
                            <li class="divider"></li>
                            <li id="m_users">
                                <a href="/admin/users">Пользователи</a>        
                            </li>
                            <li id="m_reviews">
                                <a href="/admin/reviews">Отзывы</a>        
                            </li>
                            <li id="m_company">
                                <a href="/admin/company">Компании</a>        
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/admin/default/logout">Выход</a>
                            </li>
                        </ul>					
                    </div>
                </div>
                <div class="span9">
                    <div class="well wellmain">
                        <?= $content ?>                   
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
            $("#m_<?=Yii::app()->controller->id?>").addClass("active");
        });
        </script>
    </body>
</html>  