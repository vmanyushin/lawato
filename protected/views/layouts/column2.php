﻿<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <?php echo !empty($this->h1)?'<h1>'.$this->h1.'</h1>':'';?>
	<div id="content">
		<?php echo $content; ?>
        <style>
            .operations  {display:block;padding:0;list-style-type: none}
            .operations  li {padding: 2px;}
        </style>
        <div id="sidebar">
            <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                //'title'=>'menu',
            ));
            $this->widget('zii.widgets.CMenu', array(
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
            ?>
        </div><!-- sidebar -->
	</div><!-- content -->

<?php $this->endContent(); ?>