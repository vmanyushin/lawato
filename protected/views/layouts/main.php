<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo CHtml::encode($this->page_title) ?></title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <?php Yii::app()->getClientScript()->registerScriptFile('/js/jquery.cookie.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/bootstrap.min.js', CClientScript::POS_END); ?>

    <?php Yii::app()->getClientScript()->registerCssFile('/css/bootstrap.min.css' , 'all', CClientScript::POS_HEAD );?>
    <?php Yii::app()->getClientScript()->registerCssFile('/css/nav-side-menu.min.css' , 'all', CClientScript::POS_HEAD );?>
    <?php Yii::app()->getClientScript()->registerCssFile('/css/font-awesome.min.css', 'all', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->getClientScript()->registerCssFile('/css/social-sharing.min.css', 'all', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->getClientScript()->registerCssFile('/css/styles.min.css'        , 'all', CClientScript::POS_HEAD );?>
    <?php Yii::app()->getClientScript()->registerCssFile('http://fonts.googleapis.com/css?family=Roboto:500,900,700,400', 'all', CClientScript::POS_HEAD );?>
</head>

<body <?php if (isset($this->bodyClass)) echo 'class="' . $this->bodyClass . '"' ?>>

<?php if(Yii::app()->user->hasFlash('info')):?>
    <div class="alert alert-info static-bottom" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?= Yii::app()->user->getFlash('info')?>
    </div>
<? endif ?>

<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger static-bottom" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?= print_r(Yii::app()->user->getFlash('error'),1); ?>
    </div>
<? endif ?>

<?php echo $content; ?>

<?php Yii::app()->getClientScript()->registerScript('mobile',"
var cook = $.cookie('mobile');
    var isMobile = window.matchMedia('only screen and (max-width: 480px)');

    if (isMobile.matches && (cook == undefined || cook != 'true')) {
        $.cookie('mobile', 'true');
    } else if (!isMobile.matches && cook == 'true') {
        $.removeCookie('mobile');
    }
", CClientScript::POS_LOAD);?>

<?php Yii::app()->getClientScript()->registerScript('ga',"
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-65620981-1', 'auto');
    ga('send', 'pageview');"
, CClientScript::POS_LOAD);?>

</body>
</html>
