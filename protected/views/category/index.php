<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 23.07.2015
 * Time: 7:54
 */

$this->page_title = $h1 = 'Find Laws, Legal Information, and Attorneys - LAWATO';

if (!empty($params['subcategory']) && !empty($params['city'])) {
    // all 4 params set
    $h1 = $meta['city']->name . ' ' . $meta['subcategory']->name . ', Attorneys and Law Firms - ' . $meta['state']->name;
    $this->page_title = $meta['city']->name . ' ' . $meta['subcategory']->name . ' - Local Attorneys &amp; Law Firms in ' . $meta['city']->name . ', ' . $meta['state']->abbreviation . ' | LAWATO';
    Yii::app()->clientScript->registerMetaTag($meta['city']->name . ',' . $meta['state']->abbreviation . ' ' . $meta['subcategory']->name .
        ' with detailed profiles and recommendations. Find your ' . $meta['city']->name . ',' . $meta['state']->abbreviation . ' ' . $meta['subcategory']->name .
        ' or Law Firm.', 'description');
} elseif (!empty($params['category']) && !empty($params['city'])) {
    // state + city + category
    $h1 = $meta['city']->name . ' ' . $meta['category']->name . ', Attorneys and Law Firms - ' . $meta['state']->name;
    $this->page_title = $meta['city']->name . ' ' . $meta['category']->name . ' Lawyers -  Local Attorneys &amp; Law Firms in ' . $meta['city']->name . ', ' . $meta['state']->abbreviation . ' | LAWATO';
    Yii::app()->clientScript->registerMetaTag($meta['city']->name . ', ' . $meta['state']->abbreviation . ' ' . $meta['category']->name .
        ' with detailed profiles and recommendations. Find your ' . $meta['city']->name . ',' . $meta['state']->abbreviation . ' ' . $meta['category']->name .
        ' or Law Firm.', 'description');
} elseif (!empty($params['state']) && !empty($params['subcategory'])) {
    // state + category + subcategory
    $h1 = $meta['state']->name . ': ' . $meta['subcategory']->name . ' Lawyers';
    $this->page_title = $meta['state']->name . ' ' . $meta['subcategory']->name . ': ' . $meta['state']->abbreviation . ' Lawyer, Attorney, Attorneys, Law Firms';
    Yii::app()->clientScript->registerMetaTag('Find ' . $meta['state']->name . ': ' . $meta['subcategory']->name . ' lawyers, attorneys, law firms - ' .
        $meta['state']->abbreviation . ' ' . $meta['subcategory']->name . ' Lawyers', 'description');
} elseif (!empty($params['category']) && !empty($params['subcategory']) && empty($params['city'])) {
    // category + subcategory
    $h1 = 'Browse ' . $meta['subcategory']->name . ' Lawyers';
    $this->page_title = $meta['subcategory']->name . ' Lawyer - ' . $meta['subcategory']->name . ' Attorney, Law Firm Directory | LAWATO';
    Yii::app()->clientScript->registerMetaTag('Use the largest online ' . $meta['subcategory']->name . ' lawyers directory to quickly find detailed profiles of attorneys and law firms in your area.', 'description');
} elseif (!empty($params['category']) && !empty($params['state'])) {
    // category + state
    $h1 = $meta['state']->name . ': ' . $meta['category']->name . ' Lawyers';
    $this->page_title = $meta['state']->name . ' ' . $meta['category']->name . ': ' . $meta['state']->abbreviation . ' Lawyer, Attorney, Attorneys, Law Firms';
    Yii::app()->clientScript->registerMetaTag('Find ' . $meta['state']->name . ': ' . $meta['category']->name . ' lawyers, attorneys, law firms - ' .
        $meta['state']->abbreviation . ' ' . $meta['category']->name . ' Lawyers', 'description');
} elseif (!empty($params['city'])) {
    // state + city
    $h1 = 'Find a Lawyer in ' . $meta['city']->name . ', ' . $meta['state']->name;
    $this->page_title = $meta['city']->name . ' Lawyers - Find Your ' . $meta['city']->name . ', ' . $meta['state']->abbreviation . ' Attorney or Law Firm | LAWATO';
    Yii::app()->clientScript->registerMetaTag('Use the largest online lawyers directory to find detailed profiles of qualified ' . $meta['city']->name . ',' . $meta['state']->abbreviation . 'attorneys and law firms in your area.');
} elseif (!empty($params['category'])) {
    // category
    $h1 = 'Browse ' . $meta['category']->name . ' Lawyers';
    $this->page_title = $meta['category']->name . ' Lawyer - ' . $meta['category']->name . ' Attorney, Law Firm Directory | LAWATO';
    Yii::app()->clientScript->registerMetaTag('Use the largest online ' . $meta['category']->name . ' lawyers directory to quickly find detailed profiles of attorneys and law firms in your area.', 'description');
} elseif (!empty($params['state'])) {
    // state only
    $h1 = $meta['state']->name . ' Lawyers, Attorneys, and Law Firms';
    $this->page_title = $meta['state']->name . ' Lawyers - ' . $meta['state']->name . ' (' . $meta['state']->abbreviation . ') Attorney, Law Firm Directory | LAWATO';
    Yii::app()->clientScript->registerMetaTag('Use the largest online attorney directory to quickly find detailed profiles of ' . $meta['state']->name . ' lawyers and law firms in your area.', 'description');
}

?>

<? $this->renderPartial('//shared/menu', ['params' => $params, 'displayFilter' => true]); ?>

<div class="container-fluid with-sidebar">
    <div class="row">
        <div class="col-lg-12">
            <h1><?= $h1 ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php if (count($items)): ?>
                <div class="row">
                    <? $this->renderPartial('//shared/company', ['items' => $items]); ?>
                </div> <!-- end row -->

                <? $this->renderPartial('//shared/pagination', ['pages' => $pages]); ?>
            <? endif ?>
        </div>
    </div>
</div>


