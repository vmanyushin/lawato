<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 25.07.2015
 * Time: 3:31
 */
?>

<div class="card card-container">
    <div class="main-card">
        <?php if (Yii::app()->user->isGuest == true): ?>
            <?php if ($this->beginCache('guest_profile_menu')) { ?>
<!--                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>-->
                <p id="profile-name" class="profile-name-card"></p>

                <form class="form-signin">
                    <input type="email" name="LoginForm[email]" class="form-control" placeholder="Email address"
                           required>
                    <input type="password" name="LoginForm[password]" class="form-control" required>
                    <button class="btn btn-primary btn-lawato-blue" type="submit">Sign in</button>
                    <span class="errorSummary"></span>
                    <span class="successMessage"></span>
                </form>

                <div id="uLoginContainer"
                     data-ulogin="display=buttons;fields=first_name,last_name,email,uid,sex,photo,photo_big,email;redirect_uri=http%3A%2F%2F<?= $_SERVER['HTTP_HOST'] ?>%2Fulogin%2Flogin">
                    <button class="btn btn-facebook" data-uloginbutton="facebook" alt="facebook login">
                        <i class="fa fa-facebook"></i> Facebook
                    </button>
                    <button class="btn btn-googleplus" data-uloginbutton="google" alt="google+ login">
                        <i class="fa fa-google-plus"></i> Google+
                    </button>
                </div>

                <?php  $this->widget('application.components.UloginWidget', array(
                    'params' => array(
                        'redirect' => 'http://' . $_SERVER['HTTP_HOST'] . '/ulogin/login',)));
                ?>

                <a href="javascript:;" class="forgot-password">Forgot the password?</a>
                <span class="register"> <a href="javascript:;"> Register </a></span>
                <?php $this->endCache();
            } ?>
        <?php else: ?>

            <?php if ($this->user): ?>
                <img id="profile-img" class="profile-img-card" src="<?= $this->user->photo ?>"/>
                <p id="profile-name" class="profile-name-card"><?= $this->user->full_name ?></p>
                <ul class="list-unstyled text-center user-menu">
                    <li class="<?= $_SERVER['REQUEST_URI'] == '/user/view' ? 'active' : '' ?>"><a href="/user/view">Profile</a>
                    </li>
                    <li class="<?= $_SERVER['REQUEST_URI'] == '/user/review' ? 'active' : '' ?>"><a href="/user/review">Review</a>
                    </li>
                    <li class="<?= $_SERVER['REQUEST_URI'] == '/user/profile' ? 'active' : '' ?>"><a
                            href="/company/show">Company</a></li>
                    <li><a href="/ulogin/logout?redirect_to=<?= $_SERVER['REQUEST_URI'] ?>">logout</a></li>
                </ul>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <?php if (Yii::app()->user->isGuest == true): ?>
        <?php if ($this->beginCache('guest_profile_buttons')) { ?>
            <div class="register-card">
                <form class="form form-register">

                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" name="RegisterForm[name]" class="form-control" placeholder="Full name"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" name="RegisterForm[email]" class="form-control" placeholder="Email address"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input type="password" name="RegisterForm[password]" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="repeatPassword">Repeat password</label>
                        <input type="password" name="RegisterForm[passwordRepeat]" class="form-control" required>
                    </div>

                    <span class="errorSummary"></span>
                    <span class="successMessage"></span>

                    <button class="btn btn-primary btn-lawato-blue" type="submit">Register</button>
                    <p></p>

                    <p class="text-center"><a href="javascript:backToProfile()">Return</a></p>
                </form>
            </div>

            <div class="forgot-card">
                <form class="form form-forgot">
                    <div class="form-group">
                        <input type="email" name="ResetForm[inputEmail]" class="form-control"
                               placeholder="Email address"
                               required>
                    </div>

                    <span class="errorSummary"></span>
                    <span class="successMessage"></span>
                    <button class="btn btn-primary btn-lawato-blue" type="submit">Reset password</button>
                    <p></p>

                    <p class="text-center"><a href="javascript:backToProfile()">Return</a></p>
                </form>
            </div>
            <?php $this->endCache();
        } ?>
    <? endif ?>
</div>

<?php Yii::app()->getClientScript()->registerScriptFile('/js/profile.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerCssFile('/css/profile.min.css', 'all', CClientScript::POS_HEAD); ?>
