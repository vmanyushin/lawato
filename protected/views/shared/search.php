<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 31.07.2015
 * Time: 18:30
 */
?>

<div class="row search">
    <div class="col-md-8 col-md-offset-2">

        <form action="/search" method="post" class="form form-search">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control input-lg" name="location" id="location"
                           placeholder="city, state" required>
                </div>

                <div class="col-md-6">
                    <input type="search" class="form-control input-lg typeahead" id="name" name="name"
                            placeholder="company or lawyer name" id="name" readonly/>
                </div>
            </div>

        </form>

    </div>
</div>

<?php Yii::app()->getClientScript()->registerScriptFile('/js/jquery.autocomplete.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/typeahead.bundle.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/handlebars.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->getClientScript()->registerScript('autocomplete', "
$('#location').autocomplete({
        serviceUrl: '/ajax/city',
        minLength: 1,
        limit: 5,
        onSelect: function (suggestion) {
            $('#name').removeAttr('readonly').css('background-color','rgba(255,255,255,1)').focus();
            $('.typeahead').val('');
            clearSuggestion();
        }
});
", CClientScript::POS_END); ?>

<?php Yii::app()->getClientScript()->registerScript('bloodhound',"
    function clearSuggestion()
    {
        my_Suggestion_class.clear();
        my_Suggestion_class.clearRemoteCache();
        my_Suggestion_class.initialize(true);
    }

    var my_Suggestion_class = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/ajax/company?query=%QUERY',
            filter: function (x) {
                return $.map(x, function (item) {
                    return {vval: item.name, logo: item.logo, url: item.url};
                });
            },
            wildcard: '%QUERY'
        },
        limit: 10
    });

    my_Suggestion_class.initialize();

    var typeahead_elem = $('.typeahead');
    typeahead_elem.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'vval',
            displayKey: 'vval',
            source: my_Suggestion_class.ttAdapter(),
            templates: {
                empty: '<div class=\"noitems\">No Items Found</div>',
                suggestion: Handlebars.compile('<p style=\"padding:6px\"><img width=\"70\" src=\"/images/logo/{{logo}}\"><a href=\"{{url}}\" style=\"text-decoration: none\"><span style=\"padding-left: 10px;\" class=\"typeahead-row\">{{vval}}</span></a></p>'),
                footer: Handlebars.compile('<i>Searched for \"{{query}}\"</i>')
            }
        });

    $('input').on([
        'typeahead:initialized',
        'typeahead:initialized:err',
        'typeahead:selected',
        'typeahead:autocompleted',
        'typeahead:cursorchanged',
        'typeahead:opened',
        'typeahead:closed'
    ].join(' '), function (x) {
        $(document.forms).submit();
    });
", CClientScript::POS_END); ?>