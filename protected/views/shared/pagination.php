<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 02.08.2015
 * Time: 5:53
 */
?>

<div class="row"> <!-- пагинация -->
    <div class="col-lg-4 col-lg-offset-4">
        <nav>
            <?php $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
                'nextPageLabel' => '&gt;',
                'prevPageLabel' => '&lt;',
                'maxButtonCount' => 4,
                'selectedPageCssClass' => 'active',
                'hiddenPageCssClass' => 'disabled',
                'htmlOptions' => array(
                    'class' => 'pagination',
                )
            )); ?>
        </nav>
    </div>
</div> <!-- конец пагинации -->