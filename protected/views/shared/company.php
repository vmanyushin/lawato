<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 26.07.2015
 * Time: 15:15
 */
?>

<? foreach ($items as $item): ?>
    <div class="col-lg-6 col-md-6 col-sm-12">

        <div class="panel panel-default"> <!-- panel start -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 company-item" itemtype="http://schema.org/PostalAddress" itemscope="true" itemprop="address">
                        <?php if (empty($item->logo) || !file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/logo/' . $item->logo)): ?>
                            <a href="<?= $item->urlpath ?>"><img src="/css/images/nologo.png"
                                                                 alt="<?= $item->name ?>"
                                                                 class="img-logo"
                                                                 height="124" width="124"/></a>
                        <? else: ?>
                            <a href="<?= $item->urlpath ?>"><img src="/th.php?filename=/images/logo/<?= $item->logo ?>&height=100&width=127"
                                                                 alt="<?= $item->name ?>"
                                                                 class="img-logo"
                                                                 height="124" width="124"/></a>
                        <? endif ?>

                        <div class="company-description">
                        <h3 class="company-name"><a href="<?= $item->urlpath ?>"><?= $item->name ?></a></h3>

                        <p>
                            <span itemprop="streetAddress"><?= $item->address ?></span>,
                            <span itemprop="addressLocality"><?= $item->city->name ?></span>,
                            <span itemprop="addressRegion"><?= $item->state->abbreviation ?></span>
                            <span itemprop="postalCode"><?= $item->zip_code ?></span>
                        </p>

                        <?php if(isset($item->phones[0])): ?><p class="phones"><span itemprop="telephone"><?= $item->phones[0] ?></span></p><?php endif ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- panel end -->
    </div>
<? endforeach ?>

<!-- equalizer script -->
<?php Yii::app()->getClientScript()->registerScript('equalizer',"
    $(window).resize(function(){fitCompanySize()})
    function fitCompanySize()
    {
        var max=0;
        $('.company-item').
            each(function(i,e){if($(e).height()>max) {
                $(e).css('min-height', 'auto');
                max=$(e).height();}
            }).
            each(function(i,e) { $(e).css('min-height', max); });
    }
", CClientScript::POS_END);