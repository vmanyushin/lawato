<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 23.07.2015
 * Time: 8:47
 */
?>

<?php if(!isset($displayFilter)) $displayFilter = false ?>
<!--googleoff: all-->

<div class="nav-side-menu">
    <div class="brand">
        <a href="/">
            <img src="/css/images/logo-small.png" alt="logotype" class="img-responsive">
        </a>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li>
                <? $this->renderPartial('//shared/profile'); ?>
            </li>


            <?php if($displayFilter): ?>
            <li>
                <div class="col-lg-12 filter-menu">
                    <div class="row">
                        <div class="bordered">
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'action' => Yii::app()->createUrl($this->route),
                                'method' => 'get',
                                'htmlOptions' => array(
                                    'class' => 'form-group',
                                    'name' => 'filter',
                                ),
                            )); ?>
                            <div class="form-group">
                                <?php echo CHtml::dropDownList('state', Yii::app()->request->getParam('state_id', is_object($params['state']) ? $params['state']->seourl : ''),
                                    CHtml::listData(State::model()->findAll(), 'seourl', 'name'),
                                    [
                                        'class' => 'form-control',
                                        'empty' => 'State',
                                    ]); ?>
                            </div>

                            <div class="form-group">
                                <?php if (Yii::app()->request->getParam('state_id', is_object($params['state']) ? $params['state']->id : '') > 0): ?>
                                    <?php echo CHtml::dropDownList('city', Yii::app()->request->getParam('city_id', is_object($params['city']) ? $params['city']->seourl : ''),
                                        CHtml::listData(City::model()->findAll('state_id = ' . Yii::app()->request->getParam('state_id', is_object($params['state']) ? $params['state']->id : '')), 'seourl', 'name'),
                                        [
                                            'class' => 'form-control',
                                            'empty' => 'Select city'
                                        ]); ?>
                                <?php else: ?>
                                    <select class="form-control">
                                        <option value="">City</option>
                                    </select>
                                <?php endif ?>
                            </div>

                            <div class="form-group">
                                <?php echo CHtml::dropDownList('category', Yii::app()->request->getParam('category_id', $params['category']),
                                    CHtml::listData(Category::model()->findAll(), 'id', 'name'),
                                    [
                                        'class' => 'form-control',
                                        'empty' => 'Legal Issue Category',
                                    ]); ?>
                            </div>

                            <div class="form-group">
                                <?php if (Yii::app()->request->getParam('category_id', $params['category']) > 0): ?>
                                    <?php echo CHtml::dropDownList('subcategory', Yii::app()->request->getParam('subcategory_id', $params['subcategory']),
                                        CHtml::listData(SubCategory::model()->findAll('category_id = ' . Yii::app()->request->getParam('category_id', $params['category'])), 'id', 'name'),
                                        [
                                            'class' => 'form-control',
                                            'empty' => 'Legal issue',
                                        ]); ?>
                                <?php else: ?>
                                    <select class="form-control">
                                        <option value="">Legal Issue Sub Category</option>
                                    </select>
                                <?php endif ?>
                            </div>

                            <?php $this->endWidget() ?>
                        </div>
                    </div>
                </div>
            </li>
            <?php endif; ?>

        </ul>
    </div>

    <? if($_SERVER['HTTP_HOST'] != 'lawato.loc'): ?>
        
        <!-- БЛОК РЕКЛАМЫ В ЛЕВОМ САЙДБАРЕ -->
        
    <? endif ?>
</div>

<!--googleon: all-->
<?php Yii::app()->getClientScript()->registerScriptFile('/js/filter-navigation.min.js', CClientScript::POS_END); ?>