<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 19.08.2015
 * Time: 16:23
 */
?>

<div class="review">
    <div class="avatar">
        <img src="<?= $review->user->photo ?>" alt="avatar" width="32"
             class="img-circle"/>
    </div>

    <div class="review-header <? if($review->is_published() == false) echo 'moderation'?>">
        <div>
            <a href="/profile/view/<?= $review->user->id ?>"><?= $review->user->full_name ?></a>
            <span class="time-ago"><?= Utils::timeAgo($review->created_at) ?></span>
        </div>

        <div class="rating">
            <?php foreach (range(1, $review->grade) as $number): ?>
                <span class="star"></span>
            <?php endforeach ?>
        </div>

        <?php if($review->is_published() == false): ?>
            <strong>&nbsp;this review on moderation</strong>
        <?php endif ?>
    </div>

    <div class="review-content">
        <?= $review->content ?>
    </div>
</div>

