<?php
/* @var $this AdminController */
/* @var $model Comments */

$this->breadcrumbs=array(
	'Comments'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Comments', 'url'=>array('index')),
	array('label'=>'Create Comments', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#comments-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Comments</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comments-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name'=>'user_id',
            'type'=>'raw',
            'value'=>function($data){ echo CHtml::link($data->user->full_name,'/user/user/view?id='.$data->user_id);},
        ),
		'message',
		'date',
		//'approved',
		array(
            'name'=>'game_id',
            'type'=>'raw',
            'value'=>function($data){ echo CHtml::link($data->game->getFullTitle(),$data->game->getUrl());},
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
