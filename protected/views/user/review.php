<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 19.08.2015
 * Time: 16:26
 */
?>

<? $this->renderPartial('//shared/menu'); ?>

<div class="container-fluid with-sidebar">

    <?php if (count($model) == 0): ?>
        <h1>You are no reviews at the moment</h1>

    <?php else: ?>
        <h1>Your reviews</h1>
        <hr/>

        <?php foreach ($model as $reviewCompany): ?>
            <div class="row user-review">
                <div class="col-lg-12 review-card">
                    <img src="/images/logo/<?= $reviewCompany->company->logo ?>" alt="Company logo" width="96"
                         class="img-thumbnail"/>

                    <h3><a href="<?= $reviewCompany->company->urlpath ?>"><?= $reviewCompany->company->name ?></a></h3>

                    <? $reviews = Review::model()->all()->findAll([
                        'condition' => 'company_id = :company_id',
                        'params' => [':company_id' => $reviewCompany->company_id]
                    ]); ?>

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="review-container">
                                <? foreach ($reviews as $review): ?>
                                    <?php $this->renderPartial('//shared/review', ['review' => $review]) ?>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>

    <?php endif ?>
</div>