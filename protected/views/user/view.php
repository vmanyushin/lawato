<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 06.08.2015
 * Time: 16:22
 */
?>

<? $this->renderPartial('//shared/menu'); ?>

<div class="container-fluid with-sidebar">
    <div class="row user-form">

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-2">
            <p><a href="<?=$_SESSION['REFERER']?>">return to last page</a></p>

            <label for="upload">Logo (click for upload new)</label>

            <div class="user-pic">
                <img src="<?= $model->logo ?>" alt="user logo" class="img-responsive user-logo" width="96"/>
                <div class="errorMsg"></div>
            </div>

            <form enctype="multipart/form-data" style="display: none;">
                <input type="file" name="upload" id="upload" />
            </form>

            <div class="form">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'user-form',
                    'focus' => array($model, 'full_name'),
                )); ?>

                <div class="form-group">
                    <?= $form->label($model, 'full_name'); ?>
                    <?= $form->textField($model, 'full_name', ['class' => 'form-control']); ?>
                    <?= $form->error($model, 'full_name'); ?>
                </div>

                <div class="form-group">
                    <?= $form->label($model, 'email'); ?>
                    <?= $form->emailField($model, 'email', ['class' => 'form-control']); ?>
                    <?= $form->error($model, 'email'); ?>
                </div>

                <div class="form-group">

                    <?= $form->label($model, 'gender'); ?>
                    <?= CHtml::dropDownList('UserForm[gender]', $model->gender,
                        ['F' => 'Female', 'M' => 'Male'],
                        [
                            'class' => 'form-control',
                        ]); ?>
                </div>

                <div class="form-group">
                    <?= $form->label($model, 'birthday'); ?>
                    <?= $form->dateField($model, 'birthday', ['class' => 'form-control']); ?>
                    <?= $form->error($model, 'birthday'); ?>
                </div>

                <div class="form-group">
                    <?= $form->label($model, 'password'); ?>
                    <?= $form->passwordField($model, 'password', ['class' => 'form-control']); ?>
                    <?= $form->error($model, 'password'); ?>
                </div>

                <div class="form-group">
                    <?= $form->label($model, 'passwordRepeat'); ?>
                    <?= $form->passwordField($model, 'passwordRepeat', ['class' => 'form-control']) ?>
                    <?= $form->error($model, 'passwordRepeat'); ?>
                </div>

                <?= CHtml::submitButton('Update', ['class' => 'btn btn-default btn-lawato-blue']) ?>


                <?php $this->endWidget() ?>
            </div>


        </div>

    </div>
</div>

<script src="/js/jquery.ajaxfileupload.js"></script>
<script>
    $('.user-pic > img').click(function(){
        $('#upload').click();
    })

    $('#upload').ajaxfileupload({
        'action': '/user/uploadLogo',
        'onComplete': function(response) {
            if(response.status == 'success')
                $('.user-pic > img').prop('src',response.path);
            else
                $('.user-pic .errorMsg').innerHTML = response.reason;
        }
    });

</script>