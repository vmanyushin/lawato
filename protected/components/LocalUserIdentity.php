<?php

class LocalUserIdentity extends  CUserIdentity
{
    private $id;
    private $name;
    private $isAuthenticated = false;
    private $states = array();

    public function authenticate()
    {
        $record = User::model()->findByAttributes(['email' => $this->username]);
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if (!CPasswordHelper::verifyPassword($this->password, $record->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->id = $record->id;
            $this->setState('title', $record->full_name);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getIsAuthenticated()
    {
        return $this->isAuthenticated;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPersistentStates()
    {
        return $this->states;
    }
}