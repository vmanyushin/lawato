<?php

class Utils {

    public static function slugify($text) {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');

        $text = strtolower(Utils::transliterate($text));
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return '';
        }

        return $text;
    }

    public static function transliterate($string) {
        $roman = array("Sch","sch",'Yo','Zh','Kh','Ts','Ch','Sh','Yu','Ya','yo','zh','kh','ts','ch','sh','yu','ya','A','B','V','G','D','E','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','','Y','','E','a','b','v','g','d','e','z','i','y','k','l','m','n','o','p','r','s','t','u','f','','y','','e');
        $cyrillic = array("Щ","щ",'Ё','Ж','Х','Ц','Ч','Ш','Ю','Я','ё','ж','х','ц','ч','ш','ю','я','А','Б','В','Г','Д','Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Ь','Ы','Ъ','Э','а','б','в','г','д','е','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','ь','ы','ъ','э');
        return str_replace($cyrillic, $roman, $string);
    }

    /* Recursive branch extrusion */
    static function createBranch(&$parents, $children) {
        $tree = array();
        foreach ($children as $child) {
            if (isset($parents[$child['id']])) {
                $child['children'] =
                    Utils::createBranch($parents, $parents[$child['id']]);
            }
            $tree[] = $child;
        }
        return $tree;
    }

    /* Initialization */
    public static function createTree($flat, $root = 0) {
        $parents = array();
        foreach ($flat as $a) {
            $parents[$a['parent_id']][] = $a;
        }
        return Utils::createBranch($parents, $parents[$root]);
    }

    public static function selectChild($flat, $parent_id) {
        $childs = array();

        foreach($flat as $obj) {
            if($obj['parent_id'] == $parent_id)
                $childs[] = $obj;
        }

        return $childs;
    }

    public static function cutWords($word, $len)
    {
        return preg_replace('/\s+?(\S+)?$/', '', substr($word, 0, $len + 1));
    }

    public static function timeAgo($date)
    {
        $age = '';

        if($date == null) $date = time();

        $datetime1 = new DateTime($date);
        $datetime2 = new DateTime();

        $interval = $datetime1->diff($datetime2);

        /* 0 = year
         * 1 = month
         * 2 = day
         * 3 = hour
         * 4 = minute
         * 5 = second
         *
         * example output 8m 23s
         * 0,0,0,0,8,23
         */
        $ago =  explode(',', $interval->format('%y,%m,%d,%h,%i,%s'));

        if($ago[5] == 1) $age = "a second";
        if($ago[5] > 1)  $age = "{$ago[5]} seconds";

        if($ago[4] == 1) $age = "a minute";
        if($ago[4] > 1)  $age = "{$ago[4]} minutes";

        if($ago[3] == 1) $age = "an hour";
        if($ago[3] > 1)  $age = "{$ago[3]} hours";

        if($ago[2] == 1) $age = "a day";
        if($ago[2] > 1)  $age = "{$ago[2]} days";

        if($ago[1] == 1) $age = "a month";
        if($ago[1] > 1)  $age = "{$ago[1]} months";

        if($ago[0] == 1) $age = "a year";
        if($ago[0] > 1)  $age = "{$ago[0]} years";

        $age .= ' ago';

        return $age;
    }
}