<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 27.08.2015
 * Time: 20:07
 */

class WorkHours
{
    private $hours = [
        'Sunday_from'    => '',
        'Sunday_to'      => '',
        'Monday_from'    => '',
        'Monday_to'      => '',
        'Tuesday_from'   => '',
        'Tuesday_to'     => '',
        'Wednesday_from' => '',
        'Wednesday_to'   => '',
        'Thursday_from'  => '',
        'Thursday_to'    => '',
        'Friday_from'    => '',
        'Friday_to'      => '',
        'Saturday_from'  => '',
        'Saturday_to'    => '',
    ];

    private $format12h = [
        0  => '12:00 AM',
        1  => '1:00 AM',
        2  => '2:00 AM',
        3  => '3:00 AM',
        4  => '4:00 AM',
        5  => '5:00 AM',
        6  => '6:00 AM',
        7  => '7:00 AM',
        8  => '8:00 AM',
        9  => '9:00 AM',
        10 => '10:00 AM',
        11 => '11:00 AM',
        12 => '12:00 PM',
        13 => '1:00 PM',
        14 => '2:00 PM',
        15 => '3:00 PM',
        16 => '4:00 PM',
        17 => '5:00 PM',
        18 => '6:00 PM',
        19 => '7:00 PM',
        20 => '8:00 PM',
        21 => '9:00 PM',
        22 => '10:00 PM',
        23 => '11:00 PM',
    ];

    public $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    public $format;

    public function __construct($data = null)
    {
        if ($data == null)
            return;

        if(is_array($data)) {

            foreach($this->hours as $key => $value) {
                if(!isset($data[$key]))
                    throw new Exception('Invalid workhours format, key: ' . $key . ' not found');
                $this->hours[$key] = $data[$key];
            }
        }

        else  {
            if(preg_match('/[\[\]\}\{]/', $data)) {
                $this->hours  = json_decode($data);
                $this->format = 1;
            } else {
                unset($this->hours);
                $this->format = 0;
                $this->hours  = str_replace('"', '', $data);
            }
        }
    }

    public function isSelected($weekday, $time)
    {
        if($time == '')
            return;

        if(is_object($this->hours) && $this->hours->$weekday == $time) {
            return 'selected';
        }

        if(is_array($this->hours) && $this->hours[$weekday] == $time)
            return 'selected';

    }

    public function from($day)
    {
        $day = $day . '_from';
        return $this->hours->$day != '' ? $this->format12h[$this->hours->$day] : 'closed';
    }

    public function to($day)
    {
        $day = $day . '_to';
        return $this->hours->$day != '' ? $this->format12h[$this->hours->$day] : 'closed';
    }

    public function __get($name)
    {
        return $this->hours->$name;
    }

    public function toString()
    {
        if(is_object($this->hours) || is_array($this->hours))
            return json_encode($this->hours);
        else
            return $this->hours;
    }

    public function __toString()
    {
        return $this->toString();
    }
} 