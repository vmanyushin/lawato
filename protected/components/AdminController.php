<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	 
	 
	 
	 
	public $layout='//layouts/admin';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $h1;
	public $menu=array();
	
	
	public $description = 'Welcome to Nyyell.com. USA yellow pages for you.';
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public function init()
	{
		parent::init();
	}

	protected function beforeAction($action) { 
        if( !isset($_SESSION['is_admin']) ) {
        	if( Yii::app()->controller->id !== 'default' && Yii::app()->controller->action->id !== 'login' ) {
        		$this->redirect("/admin/default/login");
        	}
        }

        return parent::beforeAction($action);
    }

	public static function makeSlug($str)
	{
		return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $str)));
	}
}