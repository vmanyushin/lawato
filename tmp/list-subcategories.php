<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 31.07.2015
 * Time: 19:54
 */


$yii    = dirname(__FILE__).'/../vendor/yiisoft/yii/framework/yii.php';
$config = dirname(__FILE__).'/../protected/config/main.php';

require_once($yii);
Yii::createWebApplication($config);


$items = SubCategory::model()->findAll();

foreach ($items as $item) {
    echo 'http://lawato.com/subcat-' . $item->id . '-' . Utils::slugify($item->name) . "\n";
}
