<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 19.08.2015
 * Time: 0:31
 */

function timeAgo($date)
{
    $age = '';

    if($date == null) $date = time();

    $datetime1 = new DateTime($date);
    $datetime2 = new DateTime();

    $interval = $datetime1->diff($datetime2);

    /* 0 = year
     * 1 = month
     * 2 = day
     * 3 = hour
     * 4 = minute
     * 5 = second
     *
     * example output 8m 23s
     * 0,0,0,0,8,23
     */
    $ago =  explode(',', $interval->format('%y,%m,%d,%h,%i,%s'));

    if($ago[5] == 1) $age = "a second";
    if($ago[5] > 1)  $age = "{$ago[5]} seconds";

    if($ago[4] == 1) $age = "a minute";
    if($ago[4] > 1)  $age = "{$ago[4]} minutes";

    if($ago[3] == 1) $age = "an hour";
    if($ago[3] > 1)  $age = "{$ago[3]} hours";

    if($ago[2] == 1) $age = "a day";
    if($ago[2] > 1)  $age = "{$ago[2]} days";

    if($ago[1] == 1) $age = "a month";
    if($ago[1] > 1)  $age = "{$ago[1]} months";

    if($ago[0] == 1) $age = "a year";
    if($ago[0] > 1)  $age = "{$ago[0]} years";

    $age .= ' ago';

    return $age;
}

echo timeAgo('2015-08-19 02:00:50');

