<?php
/**
 * Created by PhpStorm.
 * User: swop
 * Date: 26.07.2015
 * Time: 23:23
 */

date_default_timezone_set('Europe/Moscow');
set_time_limit(0);

$yii    = dirname(__FILE__).'/vendor/yiisoft/yii/framework/yii.php';
$config = dirname(__FILE__).'/protected/config/main.php';

$db = new mysqli('localhost', 'lawato', 'dgh67u8jh56y5t', 'lawato-main');

require_once($yii);
Yii::createWebApplication($config);

$limit     = 50000;
$map_index = isset($argv[1]) ? (int)$argv[1] : 0;
$offset    = $limit * $map_index;
$first     = $db->query("SELECT id FROM company ORDER BY id LIMIT 1")->fetch_array()[0];
$last      = $first + Company::model()->count();


while($first <= $last)
{
    $sitemap = new SitemapGenerator('http://lawato.com/');
    $sitemap->maxURLsPerSitemap    = $limit;
    $sitemap->sitemapFileName      = "sitemaps/sitemap_{$map_index}.xml";
    $sitemap->sitemapIndexFileName = "sitemaps/sitemap-index.xml";
    $sitemap->robotsFileName       = "robots.txt";

    echo "processing {$offset} \n";

    $mb = $first + $limit;
    $cur = $db->query("SELECT id, name FROM company WHERE id > {$first} AND id < {$mb} ORDER BY id ASC");

    echo "founded " . $cur->num_rows. "<br/>";

    while( $row = $cur->fetch_object() )
    {
        $sitemap->addUrl('http://lawato.com/' . $row->id . '-' . Utils::slugify($row->name));
    }


    $cur = null;

    $first += $limit;

    try {
        $sitemap->createSitemap( $map_index );
        $sitemap->writeSitemap();
        $sitemap->updateRobots();
    }
    catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
    $sitemap = null;
    $map_index++;
    echo "new index {$map_index} \n";
}

$first = $db->query("SELECT id FROM city ORDER BY id LIMIT 1")->fetch_array()[0];
$last  = $first + City::model()->count();

while($first <= $last)
{
    $sitemap = new SitemapGenerator('http://lawato.com/');
    $sitemap->maxURLsPerSitemap    = $limit;
    $sitemap->sitemapFileName      = "sitemaps/sitemap_{$map_index}.xml";
    $sitemap->sitemapIndexFileName = "sitemaps/sitemap-index.xml";
    $sitemap->robotsFileName       = "robots.txt";

    echo "processing {$offset} \n";

    $mb = $first + $limit;
    $cur = $db->query("SELECT id, name FROM city WHERE id > {$first} AND id < {$mb} ORDER BY id ASC");

    echo "founded " . $cur->num_rows. "<br/>";

    while( $row = $cur->fetch_object() )
    {
        $sitemap->addUrl('http://lawato.com/city-' . $row->id . '-' . Utils::slugify($row->name));
    }


    $cur = null;

    $first += $limit;

    try {
        $sitemap->createSitemap( $map_index );
        $sitemap->writeSitemap();
        $sitemap->updateRobots();
    }
    catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
    $sitemap = null;
    $map_index++;
    echo "new index {$map_index} \n";
}

