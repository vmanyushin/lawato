<?php

// require('vendor/autoload.php');

// change the following paths if necessary
$yii = null;
$config = null;

if (getenv('yii_env') == 'development') {
    $yii = dirname(__FILE__) . '/vendor/yiisoft/yii/framework/yii.php';
    $config = dirname(__FILE__) . '/protected/config/main-development.php';

    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

} else {
    $yii = dirname(__FILE__) . '/vendor/yiisoft/yii/framework/yiilite.php';
    $config = dirname(__FILE__) . '/protected/config/main.php';
}

require_once($yii);
Yii::createWebApplication($config)->run();
