/**
 * Created by swop on 25.07.2015.
 */
$(document).ready(function() {
    $('.register').click(function () {
        $('.main-card').hide();
        $('.register-card').show();
    });

    $('.forgot-password').click(function () {
        $('.main-card').hide();
        $('.forgot-card').show();
    });

    $('.form-forgot').submit(function () {
        $.ajax({
            type: 'POST',
            url: '/user/reset',
            data: $(this).serialize(),
            success: function (data) {
                if (data.forgot == 'error') {
                    $('.errorSummary').text(data.errorMsg);
                } else {
                    $('.form-forgot .errorSummary').text('');
                    $('.form-forgot .successMessage').text('Check your email');
                    $('.form-forgot input').val('');

                    setTimeout(function () {
                        $('.forgot-card').hide();
                        $('.main-card').show();
                        $('.successMessage').text('');
                    }, 3000)
                }
            }
        });

        return false;
    });

    $('.form-signin').submit(function () {
        $.ajax({
            type: 'POST',
            url: '/user/login',
            data: $(this).serialize(),
            success: function (data) {

                if (data.login == 'error') {
                    $('.form-signin .errorSummary').text(data.errorMsg);
                } else {
                    $('.form-signin .errorSummary').text('');
                    $('.form-signin .successMessage').text('You have successfully Logged in');
                    $('.form-signin input').val('');

                    setTimeout(function () {
                        location.href = location.href;
                    }, 1000)
                }
            }
        });

        return false;
    });

    $('.form-register').submit(function () {
        $.ajax({
            type: 'POST',
            url: '/user/register',
            data: $(this).serialize(),
            success: function (data) {

                if (data.register == 'error') {
                    console.log('ERROR');
                    $('.form-register .errorSummary').text(data.errorMsg);
                } else {
                    $('.form-register .errorSummary').text('');
                    $('.form-register .successMessage').text('You have successfully registered');
                    $('.form-register input').val('');

                    setTimeout(function () {
                        location.href = location.href;
                    }, 1000)
                }
            }
        });

        return false;
    });

    function backToProfile()
    {
        $('.forgot-card').hide();
        $('.register-card').hide();
        $('.main-card').show();
    }
});
