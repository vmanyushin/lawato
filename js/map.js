/**
 * Created by swop on 25.07.2015.
 */

$(document).ready(function () {

    // latitude, longitude
    var lat, lng;
    var from = '';
    var map  = '';
    var display = false;

    var travelMode  = google.maps.DirectionsTravelMode.DRIVING; // default
    var directionsService = new google.maps.DirectionsService();

    function toggle()
    {
        display = !display;
        console.log('toggle:' + display);
        if(display) {
            $('#open-map').hide();
            $('#direction-panel').fadeIn(200);
        } else {
            $('#open-map').show();
            $('#direction-panel').fadeOut(200);
        }
    }

    GMaps.geocode({
        address: address,
        callback: function (results, status) {
            if (status == 'OK') {

                var latlng = results[0].geometry.location;
                map = new GMaps({
                    div: '#map-canvas',
                    lat: latlng.lat(),
                    lng: latlng.lng(),
                    size: [1280, 800],
                    disableDefaultUI: true
                });

                map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng(),
                    title: title,

                    click: function (e) {

                    }
                });
            }
        }
    });

    $('#close-map').click(function () {
        toggle();
    })

    $('#open-map').click(function () {
        toggle();

        GMaps.geolocate({
            success: function (position) {
                lat = position.coords.latitude;
                lng = position.coords.longitude;

                var latlng = new google.maps.LatLng(lat, lng);
                geocoder = new google.maps.Geocoder();
                geocoder.geocode({'latLng': latlng}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            from = results[1].formatted_address;
                            $('#from').val(from);
                        } else {
                            $('#from').val('cannot resolve your location');
                            console.log('No results found');
                        }
                    } else {
                        $('#from').val('cannot resolve your location');
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
            },
            error: function (error) {
                alert('Geolocation failed: ' + error.message);
            },
            not_supported: function () {
                alert("Your browser does not support geolocation");
            },
            always: function () {
                console.log("Geolocation done: " + $('#from').val());
            }
        });
    });

    $('#build-direction').click(function () {
        $('#build-direction').addClass('active');
        document.getElementById('panel').innerHTML = '';
        if(typeof directionsDisplay != 'undefined') {
            directionsDisplay.setDirections({routes: []});
        }

        //directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(map.map);
        directionsDisplay.setPanel(document.getElementById('panel'));

        var request = {
            origin: $('#from').val(),
            destination: $('#to').val(),
            travelMode: travelMode
        };

        var icons = {
            start: new google.maps.MarkerImage(
                // URL
                'http://tevoz.com/images/icons/gmaps-start-icon.png',
                // (width,height)
                new google.maps.Size( 22, 40 ),
                // The origin point (x,y)
                new google.maps.Point( 0, 0 ),
                // The anchor point (x,y)
                new google.maps.Point( 10, 32 )
            ),
            end: new google.maps.MarkerImage(
                // URL
                'http://tevoz.com/images/icons/marker-direction-end.png',
                // (width,height)
                new google.maps.Size( 22, 36 ),
                // The origin point (x,y)
                new google.maps.Point( 0, 0 ),
                // The anchor point (x,y)
                new google.maps.Point( 10, 32 )
            )
        };

        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);

                var leg = response.routes[ 0 ].legs[ 0 ];
                //makeMarker( leg.start_location, icons.start, "title" );
            } else {
                document.getElementById('panel').innerHTML = 'no routes found';
            }
        });

        setTimeout(function(){
            $(".nano").nanoScroller();
        }, 1000);
    });

    $('#driving, #transit, #walking').click(function(event) {
        travelMode = google.maps.TravelMode[$(this).data('direction')];
        $('#driving, #transit, #walking').removeClass('active');
        $(this).addClass('active');
    });

    toggle();
});