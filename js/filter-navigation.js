/**
 * Created by swop on 13.08.2015.
 */
$(document).ready(function () {
    $('form[name=\'filter\']').on('submit', function () {
        var url = '';
        var a = $('#state').val() !== undefined ? parseInt(($('#state').val().split('#'))[1]) : 0;
        var b = $('#city').val() !== undefined ? parseInt(($('#city').val().split('#'))[1]) : 0;
        var c = parseInt($('#category').val());
        var d = parseInt($('#subcategory').val());

        if (a  && b  && c  && d)  url = '/state/' + a + '/city/' + b + '/category/' + c + '/subcategory/' + d;
        if (a  && b  && c  && !d) url = '/state/' + a + '/city/' + b + '/category/' + c;
        if (a  && !b && c  && d)  url = '/state/' + a + '/category/' + c + '/subcategory/' + d;
        if (a  && !b && c  && !d) url = '/state/' + a + '/category/' + c;
        if (!a && !b && c  && d)  url = '/category/' + c + '/subcategory/' + d;
        if (!a && !b && c  && !d) url = '/category/' + c;
        if (a  && b  && !c && !d) url = '/' + ($('#city').val().split('#'))[0];
        if (a  && !b && !c && !d) url = '/' + ($('#state').val().split('#'))[0];
        if (!a && !b && !c && !d) url = '/category';

        console.log('a:' + a + ' b:' + b + ' c:' + c + ' d:' + d);
        console.log('url: ' + url);

        location.href = url;
        return false;
    });

    $('#state, #city, #category, #subcategory').change(function () {
        $('form[name=\'filter\']').submit();
    });
});