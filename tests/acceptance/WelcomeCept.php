<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Looking on TITLE and META tags on Index page');

$I->amOnPage('/');
$I->see('LAWATO, Legal Information, and Attorneys - LAWATO','title');
$I->seeElement('meta[name="description"]', [ 'content' => 'Find a local lawyer and free legal information at LAWATO, the award-winning website.']);
$I->seeElement('meta[name="keywords"]',['content' => 'Lawyers, Attorney, Legal, Law']);
